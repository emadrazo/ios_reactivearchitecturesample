import RxSwift
import lmCore

public protocol HomeApiClientType {
    associatedtype Ent

    func getHome() -> Observable<Transaction<Ent>>
}

open class HomeApiClient<T>: HomeApiClientType where T:HomeEntityType{
    public typealias Ent = T

    public init(){
    }

    open func getHome() -> Observable<Transaction<T>>{
        fatalError("not implemented")
    }
    
}
