
import lmCore
import RxSwift

public class HomeApiRepository<T> where T:HomeEntityType{

    fileprivate let homeApiClient: HomeApiClient<T>

    required public init(homeApiClient: HomeApiClient<T>) {
        self.homeApiClient = homeApiClient
    }

    public func getHome() -> Observable<Transaction<T>> {
        return homeApiClient.getHome()
    }

}
