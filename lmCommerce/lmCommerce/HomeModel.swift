import lmCore
import RxSwift


public class HomeModel<T> where T:HomeEntityType{

    private let homeApiRepository: HomeApiRepository<T>

    public init(api: HomeApiRepository<T>) {
        self.homeApiRepository = api
    }

    public func getHome() -> Observable<Transaction<T>> {
       return homeApiRepository.getHome()
            .single()
    }



}
