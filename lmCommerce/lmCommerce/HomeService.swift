import lmCore
import RxSwift

public protocol HomeServiceType{
    associatedtype Ent: HomeEntityType

    init(homeModel: HomeModel<Ent>)

    func getHome()-> Observable<Transaction<Ent>>

}

open class HomeService<T>: HomeServiceType where T:HomeEntityType{
    public typealias Ent = T

    private let homeModel: HomeModel<T>

    required public init(homeModel: HomeModel<T>) {
        self.homeModel = homeModel

    }

    open func getHome() -> Observable<Transaction<T>> {
        return homeModel.getHome()
    }


}
