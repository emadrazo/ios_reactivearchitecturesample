import RxSwift

protocol ApiClientType {
    
    associatedtype Element
    
    func get(url: URL) -> Observable<Transaction<Element>>
//    func getList(url: URL) -> Observable<Transaction<[Element]>>
//    func post(url: URL, parameters: [String: Any]) -> Observable<Transaction<Element>>
//    func put(url: URL, parameters: [String: Any]) -> Observable<Transaction<Element>>
//    func delete(url: URL) -> Observable<Transaction<Element>>
}

open class ApiClient<T>: ApiClientType {
    
    public typealias Element = T
    
    public init() {
    }
    open func get(url: URL) -> Observable<Transaction<T>> {
        fatalError("get has not been implemented")
    }
//    open func getList(url: URL) -> Observable<Transaction<[T]>> {
//        fatalError("getList has not been implemented")
//    }
//    open func post(url: URL, parameters: [String: Any]) -> Observable<Transaction<T>> {
//        fatalError("post has not been implemented")
//    }
//    open func put(url: URL, parameters: [String: Any]) -> Observable<Transaction<T>> {
//        fatalError("put has not been implemented")
//    }
//    open func delete(url: URL) -> Observable<Transaction<T>> {
//        fatalError("delete has not been implemented")
//    }
}
