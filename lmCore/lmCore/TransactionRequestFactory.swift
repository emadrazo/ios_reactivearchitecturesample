import Foundation
import RxSwift

protocol TransactionRequestFactoryType {
    
    associatedtype Element
    
    func createTransaction() -> TransactionRequest<Element>
}

open class TransactionRequestFactory<T>: TransactionRequestFactoryType {
    
    public typealias Element = T
    
    public func createTransaction() -> TransactionRequest<T> {
        fatalError("perform has not been implemented")
    }
}
