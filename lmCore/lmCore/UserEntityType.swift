public protocol UserEntityType: EntityType {
    
    var identifier: String {get set}
    var name: String {get set}

    init(identifier: String, name: String)
}
