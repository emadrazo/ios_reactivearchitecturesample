public protocol HomeEntityType: EntityType {
    associatedtype BaseProduct: ProductBaseEntityType
    associatedtype ExtendedProduct: ProductExtendedEntityType

    var name: String {get set}
    var recommendedProducts: [ExtendedProduct]? {get set}
    var viewedProducts: [BaseProduct]? {get set}

    init(name: String, recommendedProducts: [ExtendedProduct]?, viewedProducts: [BaseProduct]?)
}

