import Foundation

public typealias ParametersDictionary = [String: String]

public enum Method: String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case delete  = "DELETE"
}

public struct Resource {
    public var url: URL
    public var method: Method
    public var parameters: ParametersDictionary
    
    public init(url: URL, method: Method,  parameters: ParametersDictionary) {
        self.url = url
        self.method = method
        self.parameters = parameters
    }
}
