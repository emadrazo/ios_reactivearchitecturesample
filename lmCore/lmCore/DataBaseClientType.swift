import RxSwift

public protocol DataBaseClientType {
	associatedtype Ent
	
	func findFirst() -> Observable<Transaction<Ent>>
	func findFirstEqualTo<V>(key: String, value: V) -> Observable<Transaction<Ent>>
	func findAll() -> Observable<Transaction<[Ent]>>
	func createOrUpdate(element: Ent) -> Observable<Transaction<Ent>>
	func removeFirst(key: String, value: Int) -> Observable<Transaction<Ent>>
	func removeAll() -> Observable<Transaction<Ent>>
}

open class DataBaseClientFor<T>: DataBaseClientType {
	public typealias Ent = T
	
	public init() {
	}
	
	open func findFirst() -> Observable<Transaction<T>> {
		fatalError("findFirst has not been implemented")
	}
	
	open func findFirstEqualTo<V>(key: String, value: V) -> Observable<Transaction<T>> {
		fatalError("findFirst has not been implemented")
	}
	
	open func findAll() -> Observable<Transaction<[T]>> {
		fatalError("findFirst has not been implemented")
	}
	
	open func createOrUpdate(element: T) -> Observable<Transaction<T>> {
		fatalError("findFirst has not been implemented")
	}
	
	open func removeFirst <V>(key: String, value: V) -> Observable<Transaction<T>> {
		fatalError("findFirst has not been implemented")
	}
	
	open func removeAll() -> Observable<Transaction<T>> {
		fatalError("findFirst has not been implemented")
	}
}
