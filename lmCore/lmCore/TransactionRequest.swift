import Foundation
import RxSwift

protocol TransactionRequestType {
    
    associatedtype Element
    
    func perform(observable: Observable<Element>) -> Observable<Transaction<Element>>
}

open class TransactionRequest<T>: TransactionRequestType {
    
    public typealias Element = T
    
    public func perform(observable: Observable<T>) -> Observable<Transaction<T>> {
        fatalError("perform has not been implemented")
    }
}
