import RxSwift

public protocol ApiClientType {
    
    associatedtype Ent
    
//    init()
    
    func getEntity(forResource resource: Resource) -> Observable<Transaction<Ent>>
    
    func getEntitiyList(forResource resource: Resource) -> Observable<Transaction<[Ent]>>
    
}

open class ApiClientFor<T>: ApiClientType where T: EntityType {
    
    public typealias Ent = T
    
    public init() {
    }
    
    open func getEntity(forResource resource: Resource) -> Observable<Transaction<T>> {
        fatalError("not implemented")
    }
    
    open func getEntitiyList(forResource resource: Resource) -> Observable<Transaction<[T]>> {
        fatalError("not implemented")
    }
    
}
