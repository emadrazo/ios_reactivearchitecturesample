//
//  lmCore.h
//  lmCore
//
//  Created by Eva Madrazo on 07/04/2017.
//  Copyright © 2017 Leroy Merlin. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for lmCore.
FOUNDATION_EXPORT double lmCoreVersionNumber;

//! Project version string for lmCore.
FOUNDATION_EXPORT const unsigned char lmCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <lmCore/PublicHeader.h>


