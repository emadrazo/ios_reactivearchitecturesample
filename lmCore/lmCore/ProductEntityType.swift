
public protocol ProductBaseEntityType : EntityType {
    var sku: String {get set}
    var categoryId: String {get set}
    var subcategoryId: String {get set}
    var familyId: String {get set}
    var name: String {get set}
    var image: String {get set}

    init(sku: String, categoryId:String, subcategoryId:String, familyId:String, name: String, image: String)
}

public protocol ProductExtendedEntityType : ProductBaseEntityType {
    var desc: String {get set}
    var price: Float {get set}
    var specialPrice: Float {get set}
    var discountPercentage: Float {get set}
	var favorite: Bool {get set}

	init(sku: String, categoryId:String, subcategoryId:String, familyId:String, name: String, desc: String, image: String, price: Float, specialPrice: Float, discountPercentage: Float, favorite: Bool)
}

public protocol ProductDetailEntityType: ProductExtendedEntityType {

    var longDesc: String {get set}
    init(sku: String, categoryId:String, subcategoryId:String, familyId:String, name: String, desc: String, image: String, price: Float, specialPrice: Float, discountPercentage: Float, favorite: Bool, longDesc: String)
}

