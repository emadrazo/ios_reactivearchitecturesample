import lmCore
import RxSwift

enum ProductServiceError: Error {
	case error
}

public protocol ProductServiceType{
    associatedtype Ent: ProductDetailEntityType
    
    init(productModel: ProductModel<Ent>)
    
    func getProduct(parameters: GetProductDetailParameters)-> Observable<Transaction<Ent>>
    func getProducts(withParameters parameters: GetProductsParameters) -> Observable<Transaction<[Ent]>>
    func updateProduct(parameters: UpdateProductParameters<Ent>) -> Observable<Transaction<Ent>>
}

open class ProductService<T>: ProductServiceType where T:ProductDetailEntityType {
    public typealias Ent = T
	
    private let productModel: ProductModel<T>
    
    required public init(productModel: ProductModel<T>) {
        self.productModel = productModel
    }
    
    open func getProduct(parameters: GetProductDetailParameters)-> Observable<Transaction<T>> {
        return productModel.getProduct(parameters: parameters)
    }
    
    open func getProducts(withParameters parameters: GetProductsParameters) -> Observable<Transaction<[T]>> {
        return productModel.getProducts(withParameters: parameters)
    }
    
    open func updateProduct(parameters: UpdateProductParameters<T>) -> Observable<Transaction<T>> {
        return productModel.update(parameters.product)
    }
}
