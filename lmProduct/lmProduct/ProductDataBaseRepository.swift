import Foundation
import RxSwift
import lmCore

public class ProductDataBaseRepository<T> where T:ProductBaseEntityType{

    let dataBaseClient: DataBaseClientFor<T>

    public init(dataBaseClient: DataBaseClientFor<T>) {
        self.dataBaseClient = dataBaseClient
    }

    public func getProductDetail(parameters: GetProductDetailParameters) -> Observable<Transaction<T>> {
        return dataBaseClient.findFirstEqualTo(key: "sku", value: parameters.sku)
    }

    public func getAll()->Observable<Transaction<[T]>> {
        return dataBaseClient.findAll()
    }

    public func createOrUpdate (element: T) -> Observable<Transaction<T>> {
        return dataBaseClient.createOrUpdate(element: element)
    }
}
