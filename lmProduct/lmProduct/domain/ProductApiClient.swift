import RxSwift
import lmCore

protocol ProductApiClientType {

    associatedtype Element

    func getProductDetail(sku: String) -> Observable<Transaction<ProductEntity>>
}

open class ProductApiClient<T>: ProductApiClientType {

    public typealias Element = T

    public init() {
    }
    open func getProductDetail(sku: String) -> Observable<Transaction<ProductEntity>> {
        fatalError("getProductDetail has not been implemented")
    }
}
