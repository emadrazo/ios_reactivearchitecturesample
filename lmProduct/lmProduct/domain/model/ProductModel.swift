import lmCore
import RxSwift


public class ProductModel<T> where T: ProductDetailEntityType {
    
    private let productApiRepository: ProductApiRepository<T>
    private let productDataBaseRepository: ProductDataBaseRepository<T>
    
    public init(database: ProductDataBaseRepository<T>, api: ProductApiRepository<T>) {
        self.productDataBaseRepository = database
        self.productApiRepository = api
    }

    //TODO Revisar! single no vale porque está devolviendo las dos concatenadas
    public func getProduct(parameters: GetProductDetailParameters) -> Observable<Transaction<T>> {
        return (productApiRepository.getProductDetail(parameters: parameters))
            .take(1)
            
//            productDataBaseRepository.getProductDetail(parameters: parameters)
//            .concat
        
    }
    
    public func getProducts(withParameters parameters: GetProductsParameters) -> Observable<Transaction<[T]>> {
        return productApiRepository.getProducts(withParameters: parameters)
    }
    
    public func getAll() -> Observable<Transaction<[T]>> {
        return productDataBaseRepository.getAll()
    }

	public func update(_ product: T) -> Observable<Transaction<T>> {
		return productDataBaseRepository.createOrUpdate(element: product)
	}
}
