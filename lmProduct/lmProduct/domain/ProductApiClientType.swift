import RxSwift
import lmCore

public protocol ProductApiClientType {
    
    associatedtype Ent
    
    func getProductDetail(parameters: [String: String]) -> Observable<Transaction<Ent>>
}

open class ProductApiClientFor<T>: ProductApiClientType where T:ProductDetailEntityType{

    
    public typealias Ent = T
    
    public init() {
    }
    
    open func getProductDetail(parameters: [String: String]) -> Observable<Transaction<T>>{
        fatalError("not implemented")
    }
    open func getProducts(withParameters parameters: [String: Any]) -> Observable<Transaction<[T]>> {
        fatalError("not implemented")
    }

    
}
