import Foundation
import lmCore

public struct GetProductDetailParameters {
    public let sku: String
    public let categoryId: String
    public let subcategoryId: String
    public let familyId: String
    
    public init(sku: String,
                categoryId: String = "",
                subcategoryId: String = "",
                familyId: String = "") {
        self.sku = sku
        self.categoryId = categoryId
        self.subcategoryId = subcategoryId
        self.familyId = familyId
    }
}

public struct GetProductsParameters {
    
    private static let defaultPage = 1
    private static let numberOfRows = 20
    
    public let categoryId: String
    public let subcategoryId: String
    public let familyId: String
    public let page: Int
    public let rows: Int
    public let isVariable: Bool
    public let criteria: String
    public let orderBy: String
    public let orderAscendant: Bool
    
    public init(categoryId: String,
                subcategoryId: String = "",
                familyId: String = "",
                page: Int = defaultPage,
                rows: Int = numberOfRows,
                isVariable: Bool = false,
                criteria: String = "",
                orderBy: String = "name",
                orderAscendant: Bool = true) {
        self.categoryId = categoryId
        self.subcategoryId = subcategoryId
        self.familyId = familyId
        self.page = page
        self.rows = rows
        self.isVariable = isVariable
        self.criteria = criteria
        self.orderBy = orderBy
        self.orderAscendant = orderAscendant
    }
}

public struct UpdateProductParameters<T> where T: ProductDetailEntityType {
    public let product: T
    
    public init(product: T) {
        self.product = product
    }
}
