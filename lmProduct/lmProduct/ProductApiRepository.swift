import lmCore
import RxSwift

public class ProductApiRepository<T> where T:ProductDetailEntityType{

    fileprivate let productApiClient: ProductApiClientFor<T>
    
    required public init(productApiClient: ProductApiClientFor<T>) {
        self.productApiClient = productApiClient
    }
    
    public func getProductDetail(parameters: GetProductDetailParameters) -> Observable<Transaction<T>> {
        
        var parametersDict = [String: String]()
        parametersDict["sku"] = parameters.sku
        parametersDict["categoryId"] = parameters.categoryId
        parametersDict["subcategoryId"] =  parameters.subcategoryId
        parametersDict["familyId"] = parameters.familyId
        
        return productApiClient.getProductDetail(parameters: parametersDict)
    }
    public func getProducts(withParameters parameters: GetProductsParameters) -> Observable<Transaction<[T]>> {
        
        var parametersDict = [String : Any]()
        parametersDict["categoryId"] = parameters.categoryId
        parametersDict["subcategoryId"] = parameters.subcategoryId
        parametersDict["familyId"] = parameters.familyId
        parametersDict["page"] = parameters.page
        parametersDict["rows"] = parameters.rows
        parametersDict["isVariable"] = parameters.isVariable
        parametersDict["critreria"] = parameters.criteria
        parametersDict["orderBy"] = parameters.orderBy
        parametersDict["orderAscendant"] = parameters.orderAscendant
        
        return productApiClient.getProducts(withParameters: parametersDict)
        
        
    }
}
