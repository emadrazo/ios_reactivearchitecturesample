import Foundation
import Realm
import RealmSwift
import RxSwift
import lmCore

enum TestError: Error {
	case error
}

// ENTITIES
final class ProductDetailEntityMock: Object, ProductDetailEntityType {
    dynamic var sku: String = ""
    dynamic var categoryId: String = ""
    dynamic var subcategoryId: String = ""
    dynamic var familyId: String = ""
    dynamic var name: String = ""
    dynamic var desc: String = ""
    dynamic var image: String = ""
    dynamic var price: Float = Float(0)
    dynamic var specialPrice: Float = Float(0)
    dynamic var discountPercentage: Float = Float(0)
    dynamic var favorite: Bool = false
    dynamic var longDesc: String = ""

    override public static func primaryKey() -> String? {
        return "sku"
    }

    public required init() {
        super.init()
    }

    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }

    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }

    public convenience required init(sku: String, categoryId:String, subcategoryId:String, familyId:String, name: String, desc: String, image: String, price: Float, specialPrice: Float, discountPercentage: Float, favorite: Bool){
        self.init(sku: sku, categoryId: categoryId, subcategoryId: subcategoryId, familyId: familyId, name: name, image: image)
        self.desc = desc
        self.price = price
        self.specialPrice = specialPrice
        self.discountPercentage = discountPercentage
        self.favorite = favorite
    }

    public convenience required init(sku: String, categoryId: String, subcategoryId: String, familyId: String, name: String, image: String) {
        self.init()
        self.sku = sku
        self.categoryId = categoryId
        self.subcategoryId = subcategoryId
        self.familyId = familyId
        self.name = name
        self.image = image
    }

    public convenience init(sku: String, categoryId:String, subcategoryId:String, familyId:String, name: String, desc: String, image: String, price: Float, specialPrice: Float, discountPercentage: Float, favorite: Bool, longDesc: String) {
        self.init(sku: sku, categoryId: categoryId, subcategoryId: subcategoryId, familyId: familyId, name: name, desc: desc, image: image, price: price, specialPrice: specialPrice, discountPercentage: discountPercentage, favorite: favorite, longDesc: longDesc)
    }
}

// SPIES

class DataBaseClientSpy: DataBaseClientFor<ProductDetailEntityMock> {
	
	var findFirstCalledInClient = false
	var findAllCalledInClient = false
	var createOrUpdateCalledInClient = false
	
	override func findFirstEqualTo<V>(key: String, value: V) -> Observable<Transaction<ProductDetailEntityMock>> {
		findFirstCalledInClient = true
		return Observable.just(Transaction.error(TestError.error))
	}
	
	override func findAll() -> Observable<Transaction<[Ent]>> {
		findAllCalledInClient = true
		return Observable.just(Transaction.error(TestError.error))
	}
	
	override func createOrUpdate(element: Ent) -> Observable<Transaction<Ent>> {
		createOrUpdateCalledInClient = true
		return Observable.just(Transaction.error(TestError.error))
	}
}

// PRODUCTS

let product1 = ProductDetailEntityMock(sku: "12345",
                                 categoryId: "1",
                                 subcategoryId: "1",
                                 familyId: "1",
                                 name: "New Test product",
                                 desc: "A product for testing purposes",
                                 image: "https://www.leroymerlin.es/img/r25/44/4410/441007/19730480/19730480_f1.jpg",
                                 price: 1.27,
                                 specialPrice: 1.27,
                                 discountPercentage: 0.0,
                                 favorite: false)

let product2 = ProductDetailEntityMock(sku: "54ABB",
                                 categoryId: "1",
                                 subcategoryId: "1",
                                 familyId: "1",
                                 name: "New Title Test product 3",
                                 desc: "A product for testing purposes",
                                 image: "https://www.leroymerlin.es/img/r25/44/4410/441007/19730480/19730480_f1.jpg",
                                 price: 1.27,
                                 specialPrice: 1.27,
                                 discountPercentage: 0.0,
                                 favorite: false)



