import Quick
import Nimble
import RxSwift
import RealmSwift
import lmCore

@testable import lmProduct

class ProductDataBaseRepositorySpec: QuickSpec {
	
	override func spec() {
		
		let dbClient = DataBaseClientSpy()
		
		var sut: ProductDataBaseRepository<ProductDetailEntityMock>!
		
		let bag = DisposeBag()
		
		beforeEach {
			sut = ProductDataBaseRepository(dataBaseClient: dbClient)
		}
		
		describe("Creation or modification") {
			it("should call dataBaseClient to create or update an object") {
				
				sut.createOrUpdate(element: product2).subscribe().addDisposableTo(bag)
				
				expect(dbClient.createOrUpdateCalledInClient).toEventually(equal(true))
			}
		}

		describe("Get product detail") {
			it("should call dataBaseClient to find first object with the given parameters") {
				
                let parameters = GetProductDetailParameters(sku:product1.sku,
                                                            categoryId: "1",
                                                            subcategoryId: "1",
                                                            familyId: "1")
				sut.getProductDetail(parameters: parameters).subscribe().addDisposableTo(bag)
				
				expect(dbClient.findFirstCalledInClient).toEventually(equal(true))
			}
		}

		describe("Get all products") {
			it("should  call dataBaseClient to get all products") {
				
				sut.getAll().subscribe().addDisposableTo(bag)
				
				expect(dbClient.findAllCalledInClient).toEventually(equal(true))
			}
		}
	}
}
