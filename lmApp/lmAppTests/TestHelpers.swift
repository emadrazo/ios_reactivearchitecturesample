import lmCore
import lmCommerce
import lmProduct
import Realm
import RealmSwift
import RxSwift

@testable import lmApp

//MOCKS

enum ErrorMock: Error{
    case notInitialized
	case defaultError
}

final class ProductDetailEntityMock: Object, ProductDetailEntityType {
    dynamic var sku: String = ""
    dynamic var categoryId: String = ""
    dynamic var subcategoryId: String = ""
    dynamic var familyId: String = ""
    dynamic var name: String = ""
    dynamic var desc: String = ""
    dynamic var image: String = ""
    dynamic var price: Float = Float(0)
    dynamic var specialPrice: Float = Float(0)
    dynamic var discountPercentage: Float = Float(0)
    dynamic var favorite: Bool = false
    dynamic var longDesc: String = ""

    override public static func primaryKey() -> String? {
        return "sku"
    }

    public required init() {
        super.init()
    }

    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }

    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }

    public convenience required init(sku: String, categoryId:String, subcategoryId:String, familyId:String, name: String, desc: String, image: String, price: Float, specialPrice: Float, discountPercentage: Float, favorite: Bool){
        self.init(sku: sku, categoryId: categoryId, subcategoryId: subcategoryId, familyId: familyId, name: name, image: image)
        self.desc = desc
        self.price = price
        self.specialPrice = specialPrice
        self.discountPercentage = discountPercentage
        self.favorite = favorite
    }

    public convenience required init(sku: String, categoryId: String, subcategoryId: String, familyId: String, name: String, image: String) {
        self.init()
        self.sku = sku
        self.categoryId = categoryId
        self.subcategoryId = subcategoryId
        self.familyId = familyId
        self.name = name
        self.image = image
    }

    public convenience init(sku: String, categoryId:String, subcategoryId:String, familyId:String, name: String, desc: String, image: String, price: Float, specialPrice: Float, discountPercentage: Float, favorite: Bool, longDesc: String) {
        self.init(sku: sku, categoryId: categoryId, subcategoryId: subcategoryId, familyId: familyId, name: name, desc: desc, image: image, price: price, specialPrice: specialPrice, discountPercentage: discountPercentage, favorite: favorite, longDesc: longDesc)
    }
}

extension ProductDetailEntityMock: JSONDecodable {
    func fill(dictionary: JSONDictionary) throws {
    }
}

final class HomeEntityMock: HomeEntityType {
    typealias Product = ProductDetailEntityMock

    dynamic var name: String = ""
    var recommendedProducts: [Product]? = []
    var viewedProducts: [Product]? = []

    public convenience init(name: String, recommendedProducts: [Product]?, viewedProducts: [Product]?) {
        self.init()
        self.name = name
        self.recommendedProducts = recommendedProducts
        self.viewedProducts = viewedProducts
    }
}

extension HomeEntityMock: JSONDecodable {
    func fill(dictionary: JSONDictionary) throws {
    }
}

// PRODUCTS

let product1 = ProductDetailEntityMock(sku: "12345",
                                 categoryId: "1",
                                 subcategoryId: "1",
                                 familyId: "1",
                                 name: "New Test product",
                                 desc: "A product for testing purposes",
                                 image: "https://www.leroymerlin.es/img/r25/44/4410/441007/19730480/19730480_f1.jpg",
                                 price: 1.27,
                                 specialPrice: 1.27,
                                 discountPercentage: 0.0,
                                 favorite: false)

let product2 = ProductDetailEntityMock(sku: "54ABB",
                                 categoryId: "1",
                                 subcategoryId: "1",
                                 familyId: "1",
                                 name: "New Title Test product 3",
                                 desc: "A product for testing purposes",
                                 image: "https://www.leroymerlin.es/img/r25/44/4410/441007/19730480/19730480_f1.jpg",
                                 price: 1.27,
                                 specialPrice: 1.27,
                                 discountPercentage: 0.0,
                                 favorite: false)


//SPIES

final class HomeServiceSpy<T>: HomeService<T> where T:HomeEntityType, T:JSONDecodable{
    public typealias Ent = T

    var getHomeCalled:Bool = false

    public convenience init(){
        self.init(homeModel: HomeModel(api: HomeApiRepository(homeApiClient: HomeApiClient () ) ) )
    }

    required public init(homeModel: HomeModel<T>) {
        super.init(homeModel: homeModel)
    }

    public override func getHome() -> Observable<Transaction<T>> {
        getHomeCalled = true

        return Observable.just(Transaction.error(ErrorMock.notInitialized))
    }
}

final class ProductServiceSpy<T>: ProductService<T> where T:ProductDetailEntityType, T:Object, T:JSONDecodable {
	public typealias Ent = T
	
	var getListCalled:Bool = false
	var getProductDetailCalled:Bool = false
	var updateProductCalled:Bool = false
	
	public convenience init() {
		self.init(productModel: ProductModel(database: ProductDataBaseRepository(dataBaseClient: DataBaseClient<T>()),
		                                     api: ProductApiRepository(productApiClient: ProductApiClient<T>(apiClient: ApiClient<T>()))))
	}
	
	required public init(productModel: ProductModel<T>) {
		super.init(productModel: productModel)
	}
	
	public override func getProducts(withParameters parameters: GetProductsParameters) -> Observable<Transaction<[T]>> {
		getListCalled = true
		return Observable.just(Transaction.error(ErrorMock.notInitialized))
	}
	
	public override func getProduct(parameters: GetProductDetailParameters) -> Observable<Transaction<T>> {
		getProductDetailCalled = true
		return Observable.just(Transaction.error(ErrorMock.notInitialized))
	}
	
	public override func updateProduct(parameters: UpdateProductParameters<T>) -> Observable<Transaction<T>> {
		updateProductCalled = true
		return Observable.just(Transaction.error(ErrorMock.notInitialized))
	}
}

class ProductListRouterSpy: ProductListRouter {
	var navigateToDetailCalledInRouter = false
	override func navigateToDetail(product: ProductEntityType) {
		navigateToDetailCalledInRouter = true
	}
}

class ProductDetailRouterSpy: ProductDetailRouter {
}

class HomeRouterSpy: HomeRouter {
	var navigateToDetailCalledInRouter = false
	override func navigateToProductDetail(product: ProductEntity) {
		navigateToDetailCalledInRouter = true
	}
}

class GetProductsWithParametersUseCaseSuccessMockAndSpy: GetProductsWithParametersUseCase<ProductEntity> {
	var executeCalledInUseCase = false
	override func execute(withParameters parameters: GetProductsParameters) -> Observable<Transaction<[ProductEntity]>> {
		executeCalledInUseCase = true
		return Observable.of(Transaction.success([ProductEntity(), ProductEntity()]))
	}
}

class GetProductsWithParametersUseCaseErrorMockAndSpy: GetProductsWithParametersUseCase<ProductEntity> {
	var executeCalledInUseCase = false
	override func execute(withParameters parameters: GetProductsParameters) -> Observable<Transaction<[ProductEntity]>> {
		executeCalledInUseCase = true
		return Observable.of(Transaction.error(ErrorMock.defaultError))
	}
}

class GetProductDetailUseCaseSuccessMockAndSpy: GetProductDetailUseCase<ProductEntity> {
	var executeCalledInUseCase = false
	override func execute(parameters: GetProductDetailParameters) -> Observable<Transaction<ProductEntity>> {
		executeCalledInUseCase = true
		return Observable.of(Transaction.success(ProductEntity()))
	}
}

class GetProductDetailUseCaseErrorMockAndSpy: GetProductDetailUseCase<ProductEntity> {
	var executeCalledInUseCase = false
	override func execute(parameters: GetProductDetailParameters) -> Observable<Transaction<ProductEntity>> {
		executeCalledInUseCase = true
		return Observable.of(Transaction.error(ErrorMock.defaultError))
	}
}

class UpdateProductUseCaseSuccessMockAndSpy: UpdateProductUseCase<ProductEntity> {
	var executeCalledInUseCase = false
	override func execute(withParameters parameters: UpdateProductParameters<ProductEntity>) -> Observable<Transaction<ProductEntity>> {
		executeCalledInUseCase = true
		return Observable.of(Transaction.success(ProductEntity()))
	}
}

class UpdateProductUseCaseErrorMockAndSpy: UpdateProductUseCase<ProductEntity> {
	var executeCalledInUseCase = false
	override func execute(withParameters parameters: UpdateProductParameters<ProductEntity>) -> Observable<Transaction<ProductEntity>> {
		executeCalledInUseCase = true
		return Observable.of(Transaction.error(ErrorMock.defaultError))
	}
}

class GetHomeUseCaseSuccessMockAndSpy: GetHomeUseCase<HomeEntity> {
	var executeCalledInUseCase = false
	override func execute() -> Observable<Transaction<HomeEntity>> {
		executeCalledInUseCase = true
		return Observable.of(Transaction.success(HomeEntity()))
	}
}

class GetHomeUseCaseErrorMockAndSpy: GetHomeUseCase<HomeEntity> {
	var executeCalledInUseCase = false
	override func execute() -> Observable<Transaction<HomeEntity>> {
		executeCalledInUseCase = true
		return Observable.of(Transaction.error(ErrorMock.defaultError))
	}
}


