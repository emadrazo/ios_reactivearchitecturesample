import Quick
import Nimble
import RealmSwift
import RxSwift

import lmCore
import lmProduct

@testable import lmApp

class GetProductsWithParametersUseCaseSpec: QuickSpec {
	
	override func spec() {
		
		let serviceMock = ProductServiceSpy<ProductEntityMock>()
		let parameters = GetProductsParameters(categoryId: "1",
		                                       subcategoryId: "1",
		                                       familyId: "1",
		                                       page: 1,
		                                       rows: 20,
		                                       isVariable: false,
		                                       criteria: "",
		                                       orderBy: "name",
		                                       orderAscendant: true)

		let sut = GetProductsWithParametersUseCase(productService: serviceMock)
		
		let bag = DisposeBag()
		
		describe("Execution") {
			it("shoud call service") {

				sut.execute(withParameters: parameters).subscribe().addDisposableTo(bag)
				
				expect(serviceMock.getListCalled).toEventually(equal(true))
			}
		}
	}
}
