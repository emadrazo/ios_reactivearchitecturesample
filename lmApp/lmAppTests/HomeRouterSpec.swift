import Quick
import Nimble

import lmCore
import lmProduct
import lmCommerce

@testable import lmApp

class HomeRouterSpec: QuickSpec {
	
	override func spec() {
		var navVC: UINavigationController!
		var sut: HomeRouter!
		
		beforeEach {
			navVC = UINavigationController()
			sut = HomeRouter(navigationController: navVC)
			
			UIApplication.shared.keyWindow!.rootViewController = navVC
			let _ = navVC.view
		}
		
		describe("Start") {
			it("should start with a HomeViewController") {
				
				sut.start()
				
				expect(navVC.topViewController).to(beAnInstanceOf(HomeViewController.self))
			}
		}
		
		describe("Navigation") {
			it("should push a ProductDetailViewController when navigateToProductDetail is called") {
				
				sut.navigateToProductDetail(product: ProductEntity())
				
				expect(navVC.topViewController).to(beAnInstanceOf(ProductDetailViewController.self))
			}
			
			//TODO: tests del resto de métodos de navegación, cuadno existan
		}
	}
}
