import Quick
import Nimble
import RealmSwift
import RxSwift

import lmProduct
import lmCommerce

@testable import lmApp

class HomeViewModelSpec: QuickSpec {
	
	override func spec() {
		
		let homeService = HomeServiceSpy<HomeEntity>()
		
		let succestUseCase = GetHomeUseCaseSuccessMockAndSpy(homeService: homeService)
		let errorUseCase = GetHomeUseCaseErrorMockAndSpy(homeService: homeService)
		
		let router = HomeRouterSpy(navigationController: nil)
		
		var sut: HomeViewModel!
		
		let bag = DisposeBag()
		
		beforeEach {
			sut = HomeViewModel(router: router, useCase: succestUseCase)
		}
		
		describe("Execution") {
			it("should execute use case") {
				
				sut.reloadHome()
				
				expect(succestUseCase.executeCalledInUseCase).toEventually(equal(true))
			}
		}
		
		describe("Navigation") {
			it("should call router to navigate to detail") {
				
				sut.didSelectProduct(ProductEntity())
				
				expect(router.navigateToDetailCalledInRouter).toEventually(equal(true))
			}
		}
		
		describe("Response") {
			it("should provide a data observable on success") {
				
				var dataReceived = false
				
				sut.recommendedProductsObservable
					.skip(1)
					.subscribe(onNext: { _ in
						dataReceived = true
					})
					.addDisposableTo(bag)
				
				sut.reloadHome()
				
				expect(dataReceived).toEventually(equal(true), timeout: 3)
			}
			
			it("should provide an error observable on error") {
				
				sut = HomeViewModel(router: router, useCase: errorUseCase)
				
				var errorReceived = false
				
				sut.errorObservable
					.subscribe(onNext: { _ in
						errorReceived = true
					})
					.addDisposableTo(bag)
				
				sut.reloadHome()
				
				expect(errorReceived).toEventually(equal(true), timeout: 3)
			}
		}
	}
}
