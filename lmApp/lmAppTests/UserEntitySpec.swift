import Quick
import Nimble

@testable import lmApp

class UserEntitySpec: QuickSpec {
    override func spec() {

        var sut = UserEntity(identifier: "identifier", name: "name")

        beforeEach {
            sut = UserEntity(identifier: "identifier", name: "name")
        }

        describe("Creation") {
            it("shoud return the expected object after creating it") {

                expect(sut.identifier).to(match("identifier"))
                expect(sut.name).to(match("name"))
            }
        }
    }
}


