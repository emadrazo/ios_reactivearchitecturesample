import Quick
import Nimble
import RealmSwift
import RxSwift

import lmCore
import lmProduct

@testable import lmApp

class GetProductListUseCaseSpec: QuickSpec {
	
	override func spec() {
		
		let serviceMock = ProductServiceSpy<ProductDetailEntityMock>()
		let sut = GetProductListUseCase(productService: serviceMock)
		
		let bag = DisposeBag()
		
		describe("Execution") {
			it("shoud call service") {
				
				sut.execute().subscribe().addDisposableTo(bag)
				
				expect(serviceMock.getListCalled).toEventually(equal(true))
			}
		}
	}
}
