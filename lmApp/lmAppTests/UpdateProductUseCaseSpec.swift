import Quick
import Nimble
import RealmSwift
import RxSwift

import lmCore
import lmProduct

@testable import lmApp

class UpdateProductUseCaseSpec: QuickSpec {
	
	override func spec() {
		let serviceMock = ProductServiceSpy<ProductDetailEntityMock>()
		let product = ProductDetailEntityMock()
		let parameters = UpdateProductParameters(product: product)
		let sut = UpdateProductUseCase(productService: serviceMock)
		
		let bag = DisposeBag()
		
		describe("Execution") {
			it("shoud call service") {
				
				sut.execute(withParameters: parameters).subscribe().addDisposableTo(bag)
				
				expect(serviceMock.updateProductCalled).toEventually(equal(true))
			}
		}
	}
}
