import Quick
import Nimble
import RealmSwift
import RxSwift

import lmCore
import lmProduct

@testable import lmApp

class GetProductDetailUseCaseSpec: QuickSpec {
	
	override func spec() {
		let parameters = GetProductDetailParameters(sku: "sku", categoryId: "1", subcategoryId: "1", familyId: "1")
		let serviceMock = ProductServiceSpy<ProductDetailEntityMock>()
		let sut = GetProductDetailUseCase(productService: serviceMock)
		
		let bag = DisposeBag()
		
		describe("Execution") {
			it("shoud call service") {
				
				sut.execute(parameters: parameters).subscribe().addDisposableTo(bag)
				
				expect(serviceMock.getProductDetailCalled).toEventually(equal(true))
			}
		}
	}
}
