import Quick
import Nimble
import lmCore
import RealmSwift
import RxSwift

@testable import lmApp



class DataBaseClientSpec: QuickSpec {
	
	private func sample1()-> ProductDetailEntityMock{
		return ProductDetailEntityMock(sku: "sku1", categoryId: "1", subcategoryId: "1", familyId: "1", name: "name1", desc: "desc1", image: "image1", price: 1.9, specialPrice: 1.8, discountPercentage: 1.0, favorite: true)
	}
	private func sample2()-> ProductDetailEntityMock{
		return ProductDetailEntityMock(sku: "sku2", categoryId: "1", subcategoryId: "1", familyId: "1", name: "name2", desc: "desc2", image: "image2", price: 21.95, specialPrice: 21.8, discountPercentage: 1.0, favorite: true)
	}
	private func sample2Repeated()-> ProductDetailEntityMock{
		return ProductDetailEntityMock(sku: "sku2", categoryId: "1", subcategoryId: "1", familyId: "1",  name: "other name2", desc: "other desc2", image: "other image2", price: 20.95, specialPrice: 20.8, discountPercentage: 1.3, favorite: true)
	}
	
	override func spec() {
		
		var sut = DataBaseClient<ProductDetailEntityMock>()
		
		let bag = DisposeBag()
		
		beforeEach {
			sut = DataBaseClient<ProductDetailEntityMock>()
		}
		
		afterEach {
			sut.removeAll().subscribe().addDisposableTo(bag)
		}
		
		describe("Creation") {
			it("shoud create object") {
				
				let p1 = self.sample1()
				
				sut.createOrUpdate(element: p1).subscribe().addDisposableTo(bag)
				
				var result:Transaction<ProductDetailEntityMock> = Transaction.error(ErrorMock.notInitialized)
				
				sut.findFirst().single().subscribe(onNext: {
					result = $0
					
				}).addDisposableTo(bag)
				
				expect(result.value?.sku).toEventually(equal("sku1"))
				expect(result.value?.name).to(equal(p1.name))
				expect(result.value?.desc).to(equal(p1.desc))
				expect(result.value?.image).to(equal(p1.image))
				expect(result.value?.price).to(equal(p1.price))
				expect(result.value?.specialPrice).to(equal(p1.specialPrice))
				expect(result.value?.discountPercentage).to(equal(p1.discountPercentage))
				expect(result.value?.favorite).to(equal(p1.favorite))
			}
			
			it("should update when primary key exists") {
				
				let p2 = self.sample2()
				let otherP2 = self.sample2Repeated()
				
				sut.createOrUpdate(element: p2).subscribe().addDisposableTo(bag)
				sut.createOrUpdate(element: otherP2).subscribe().addDisposableTo(bag)
				
				var result:Transaction<[ProductDetailEntityMock]> = Transaction.error(ErrorMock.notInitialized)
				
				sut.findAll().subscribe(onNext: {
					result = $0
				}).addDisposableTo(bag)
				
				expect(result.value?.count).to(equal(1))
				expect(result.value?[0].sku).to(equal("sku2"))
				expect(result.value?[0].name).to(equal(otherP2.name))
				expect(result.value?[0].desc).to(equal(otherP2.desc))
				expect(result.value?[0].image).to(equal(otherP2.image))
				expect(result.value?[0].price).to(equal(otherP2.price))
				expect(result.value?[0].specialPrice).to(equal(otherP2.specialPrice))
				expect(result.value?[0].discountPercentage).to(equal(otherP2.discountPercentage))
				expect(result.value?[0].favorite).to(equal(otherP2.favorite))
			}
			
			it("change created object does not affect database") {
				
				let p2 = self.sample2()
				sut.createOrUpdate(element: p2).subscribe().addDisposableTo(bag)
				
				let oldP2Name = p2.name
				p2.name = "other name for this object"
				
				var result:Transaction<ProductDetailEntityMock> = Transaction.error(ErrorMock.notInitialized)
				
				sut.findFirst().single().subscribe(onNext: {
					result = $0
				}).addDisposableTo(bag)
				
				expect(result.value?.sku).to(equal(p2.sku))
				expect(result.value?.name).toNot(equal(p2.name))
				expect(result.value?.name).to(equal(oldP2Name))
			}
			
			it("change removed object does not affect database") {
				
				let p2 = self.sample2()
				sut.createOrUpdate(element: p2).subscribe().addDisposableTo(bag)
				sut.removeFirst(key: "sku", value: p2.sku).subscribe().addDisposableTo(bag)
				
				p2.name = "other name for this object"
				
				expect(p2.name).to(equal("other name for this object"))
			}
		}
		
		describe("Find objects with key") {
			it("shoud find first object") {
				
				let p1 = self.sample1()
				let p2 = self.sample2()
				
				sut.createOrUpdate(element: p2).subscribe().addDisposableTo(bag)
				sut.createOrUpdate(element: p1).subscribe().addDisposableTo(bag)
				
				var result:Transaction<ProductDetailEntityMock> = Transaction.error(ErrorMock.notInitialized)
				
				sut.findFirstEqualTo(key: "sku", value: p1.sku).subscribe(onNext: {
					result = $0

				}).addDisposableTo(bag)
				
				expect(result.value?.sku).toEventually(equal("sku1"))
			}
		}
		
		describe("Find objects") {
			it("shoud find first object") {
				
				let p1 = self.sample1()
				let p2 = self.sample2()
				
				sut.createOrUpdate(element: p2).subscribe().addDisposableTo(bag)
				sut.createOrUpdate(element: p1).subscribe().addDisposableTo(bag)
				
				var result:Transaction<[ProductDetailEntityMock]> = Transaction.error(ErrorMock.notInitialized)
				
				sut.findAll().subscribe(onNext: {
					result = $0
					})
					.addDisposableTo(bag)
				
				expect(result.value?.count).to(equal(2))
				
				//TODO chequear que el array contiene p1 y p2 en cualquier orden
			}
		}
		
		describe("Removing objects") {
			it("shoud remove objects by key with empty database") {
				
				let p1 = self.sample1()
				
				sut.createOrUpdate(element: p1).subscribe().addDisposableTo(bag)
				sut.removeFirst(key: "sku", value: p1.sku).subscribe().addDisposableTo(bag)
				
				var noElementFound:Bool = true
				
				waitUntil(timeout: 3) { done in
					sut.findFirst()
						.subscribe(onNext: { transaction in
							noElementFound = false
						}, onCompleted: {
							expect(noElementFound).to(equal(true))
							done()
						})
						.addDisposableTo(bag)
				}
			}
			
			it("shoud remove objects by key with current database") {
				
				let p1 = self.sample1()
				let p2 = self.sample2()
				
				sut.createOrUpdate(element: p2).subscribe().addDisposableTo(bag)
				sut.createOrUpdate(element: p1).subscribe().addDisposableTo(bag)
				sut.removeFirst(key: "sku", value: p1.sku).subscribe().addDisposableTo(bag)
				
				var noElementFound:Bool = true
				
				waitUntil(timeout: 3) { done in
					sut.findFirstEqualTo(key: "sku", value: p1.sku)
						.subscribe(onNext: { transaction in
							noElementFound = false
						}, onCompleted: {
							expect(noElementFound).to(equal(true))
							done()
						})
						.addDisposableTo(bag)
				}
			}
			
			it("shoud remove all objects") {
				
				let p1 = self.sample1()
				let p2 = self.sample2()
				
				sut.createOrUpdate(element: p1).subscribe().addDisposableTo(bag)
				sut.createOrUpdate(element: p2).subscribe().addDisposableTo(bag)
				sut.removeAll().subscribe().addDisposableTo(bag)
				
				var noElementFound:Bool = true
				
				waitUntil(timeout: 3) { done in
					sut.findFirst()
						.subscribe(onNext: { transaction in
							noElementFound = false
						}, onCompleted: {
							expect(noElementFound).to(equal(true))
							done()
						})
						.addDisposableTo(bag)
				}
			}
		}
	}
}

