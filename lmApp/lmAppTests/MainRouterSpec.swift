import Quick
import Nimble

import lmCore
import lmProduct

@testable import lmApp

class MainRouterSpec: QuickSpec {
	
	override func spec() {
		var sut: MainRouter!
		
		beforeEach {
			sut = MainRouter(window: UIApplication.shared.keyWindow!)
		}
		
		describe("Start") {
			it("should start with a MainTabBarController") {
				
				sut.start()
				
				expect(UIApplication.shared.keyWindow!.rootViewController).to(beAnInstanceOf(MainTabBarController.self))
			}

			it("MainTabBarController should have 5 tabs") {
				
				sut.start()
				
				let mainTabController = UIApplication.shared.keyWindow!.rootViewController as! MainTabBarController
				
				expect(mainTabController.viewControllers?.count).to(equal(5))
			}

			it("tabs should be displayed in order") {
				
				sut.start()
				
				let mainTabController = UIApplication.shared.keyWindow!.rootViewController as! MainTabBarController
				
				expect((mainTabController.viewControllers?[0] as? UINavigationController)?.topViewController).to(beAnInstanceOf(HomeViewController.self))
				expect((mainTabController.viewControllers?[1] as? UINavigationController)?.topViewController).to(beAnInstanceOf(ProductListViewController.self))
				// TODO: falta comprobar el resto de tabs cuando tengan ViewController propio
			}
		}
	}
}
