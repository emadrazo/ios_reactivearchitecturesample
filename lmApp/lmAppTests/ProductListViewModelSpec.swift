import Quick
import Nimble
import RealmSwift
import RxSwift

import lmProduct

@testable import lmApp

class ProductListViewModelSpec: QuickSpec {
	
	override func spec() {
		
		let productService = ProductServiceSpy<ProductDetailEntity>()
		
		let succestUseCase = GetProductsWithParametersUseCaseSuccessMockAndSpy(productService: productService)
		
		let router = ProductListRouterSpy(navigationController: nil)
		
		var sut: ProductListViewModel!
		
		let bag = DisposeBag()
		
		beforeEach {
			sut = ProductListViewModel(router: router, getProductsWithParametersUseCase: succestUseCase)
		}
		
		describe("Navigation") {
			it("should call router to navigate to detail") {
				
				sut.productSelected(product1)
				
				expect(router.navigateToDetailCalledInRouter).toEventually(equal(true))
			}
		}
		
		describe("Execution") {
			it("should execute use case") {
				
				sut.realoadList()
				
				expect(succestUseCase.executeCalledInUseCase).toEventually(equal(true))
			}
		}

		describe("Response") {
			it("should provide a data observable on success") {
				
				var dataReceived = false
				
				sut.productsObservable
					.skip(1)
					.subscribe(onNext: { _ in
						dataReceived = true
					})
					.addDisposableTo(bag)
				
				sut.realoadList()
				
				expect(dataReceived).toEventually(equal(true), timeout: 3)
			}

			it("should provide an error observable on error") {
				
				let errorUseCase = GetProductsWithParametersUseCaseErrorMockAndSpy(productService: productService)
				
				sut = ProductListViewModel(router: router, getProductsWithParametersUseCase: errorUseCase)
				
				var errorReceived = false
				
				sut.errorObservable
					.subscribe(onNext: { _ in
						errorReceived = true
					})
					.addDisposableTo(bag)

				sut.realoadList()
				
				expect(errorReceived).toEventually(equal(true), timeout: 3)
			}
		}
	}
}
