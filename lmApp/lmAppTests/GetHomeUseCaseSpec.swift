
import Quick
import Nimble
import lmCore
import RealmSwift
import RxSwift

import lmCommerce

@testable import lmApp









class GetHomeUseCaseSpec: QuickSpec {

    override func spec() {

        let serviceMock = HomeServiceSpy<HomeEntityMock>()
        let sut = GetHomeUseCase(homeService: serviceMock)

        let bag = DisposeBag()

        beforeEach {

        }

        afterEach {

        }

        describe("Execution") {
            it("shoud call service") {

                sut.execute().subscribe().addDisposableTo(bag)

                expect(serviceMock.getHomeCalled).toEventually(equal(true))

            }

        }
    }
}


