import Quick
import Nimble
import RealmSwift
import RxSwift

import lmProduct

@testable import lmApp

class ProductDetailViewModelSpec: QuickSpec {
	
	override func spec() {
		
		let productService = ProductServiceSpy<ProductEntity>()
		
		let getProductSuccestUseCase = GetProductDetailUseCaseSuccessMockAndSpy(productService: productService)
		let getProductErrorUseCase = GetProductDetailUseCaseErrorMockAndSpy(productService: productService)
		
		let updateProductSuccestUseCase = UpdateProductUseCaseSuccessMockAndSpy(productService: productService)
		let updateProductErrorUseCase = UpdateProductUseCaseErrorMockAndSpy(productService: productService)

		let router = ProductDetailRouterSpy(navigationController: nil, product: ProductEntityMock())
		
		let parameters = GetProductDetailParameters(sku: "1", categoryId: "", subcategoryId: "", familyId: "")
		
		var sut: ProductDetailViewModel!
		
		let bag = DisposeBag()
		
		beforeEach {
			sut = ProductDetailViewModel(router: router,
			                             getProductUseCase: getProductSuccestUseCase,
			                             updateProductUseCase: updateProductSuccestUseCase,
			                             parameters: parameters)
		}
		
		describe("Execution") {
			it("should execute scene use case") {
				
				sut.reloadDetail()
				
				expect(getProductSuccestUseCase.executeCalledInUseCase).toEventually(equal(true))
			}
			
			it("should execute set favorite use case") {
				
				sut.favoriteDidChange()
				
				expect(updateProductSuccestUseCase.executeCalledInUseCase).toEventually(equal(true))
			}
		}
		
		describe("Response") {
			it("should provide a name observable on success") {
				
				var dataReceived = false
				
				sut.productName
					.skip(1)
					.subscribe(onNext: { _ in
						dataReceived = true
					})
					.addDisposableTo(bag)
				
				sut.reloadDetail()
				
				expect(dataReceived).toEventually(equal(true), timeout: 3)
			}
			
			it("should provide a description observable on success") {
				
				var dataReceived = false
				
				sut.productDescription
					.skip(1)
					.subscribe(onNext: { _ in
						dataReceived = true
					})
					.addDisposableTo(bag)
				
				sut.reloadDetail()
				
				expect(dataReceived).toEventually(equal(true), timeout: 3)
			}

			it("should provide a favorite observable on success") {
				
				var dataReceived = false
				
				sut.productIsFavorite
					.skip(1)
					.subscribe(onNext: { _ in
						dataReceived = true
					})
					.addDisposableTo(bag)
				
				sut.reloadDetail()
				
				expect(dataReceived).toEventually(equal(true), timeout: 3)
			}

			it("should provide an error message observable when an error occur in scene use case") {
				
				sut = ProductDetailViewModel(router: router,
				                             getProductUseCase: getProductErrorUseCase,
				                             updateProductUseCase: updateProductSuccestUseCase,
				                             parameters: parameters)
				
				var errorReceived = false
				
				sut.errorMesssage
					.subscribe(onNext: { _ in
						errorReceived = true
					})
					.addDisposableTo(bag)
				
				sut.reloadDetail()
				
				expect(errorReceived).toEventually(equal(true), timeout: 3)
			}

			it("should provide an error message observable when an error occur in set favorite use case") {
				
				sut = ProductDetailViewModel(router: router,
				                             getProductUseCase: getProductErrorUseCase,
				                             updateProductUseCase: updateProductErrorUseCase,
				                             parameters: parameters)
				
				var errorReceived = false
				
				sut.errorMesssage
					.subscribe(onNext: { _ in
						errorReceived = true
					})
					.addDisposableTo(bag)
				
				sut.favoriteDidChange()
				
				expect(errorReceived).toEventually(equal(true), timeout: 3)
			}
		}
	}
}
