import Quick
import Nimble

import lmCore
import lmProduct

@testable import lmApp

class ProductListRouterSpec: QuickSpec {
	
	override func spec() {
		var navVC: UINavigationController!
		var sut: ProductListRouter!
		
		beforeEach {
			navVC = UINavigationController()
			sut = ProductListRouter(navigationController: navVC)

			UIApplication.shared.keyWindow!.rootViewController = navVC
			let _ = navVC.view
		}
		
		describe("Start") {
			it("should start with a ProductListViewController") {
				
				sut.start()
				
				expect(navVC.topViewController).to(beAnInstanceOf(ProductListViewController.self))
			}
		}

		describe("Navigation") {
			it("should push a ProductDetailViewController when navigateToDetail is called") {
				
				sut.navigateToDetail(product: ProductEntityMock())
				
				expect(navVC.topViewController).to(beAnInstanceOf(ProductDetailViewController.self))
			}
		}
	}
}
