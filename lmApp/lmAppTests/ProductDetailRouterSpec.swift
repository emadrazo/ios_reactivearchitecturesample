import Quick
import Nimble

import lmCore
import lmProduct

@testable import lmApp

class ProductDetailRouterSpec: QuickSpec {
	
	override func spec() {
		var navVC: UINavigationController!
		var sut: ProductDetailRouter!
		
		beforeEach {
			navVC = UINavigationController()
			sut = ProductDetailRouter(navigationController: navVC, product: ProductEntityMock())
			
			UIApplication.shared.keyWindow!.rootViewController = navVC
			let _ = navVC.view
		}
		
		describe("Start") {
			it("should start with a ProductDetailViewController") {
				
				sut.start()
				
				expect(navVC.topViewController).to(beAnInstanceOf(ProductDetailViewController.self))
			}
		}
	}
}
