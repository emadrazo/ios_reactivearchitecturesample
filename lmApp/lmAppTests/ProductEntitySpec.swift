import Quick
import Nimble

@testable import lmApp

class ProductDetailEntitySpec: QuickSpec {
	override func spec() {
		
		var sut: ProductDetailEntity!
		
		beforeEach {
			sut = ProductDetailEntity(sku: "sku",
			                    categoryId: "1",
			                    subcategoryId: "1",
			                    familyId: "1",
			                    name: "name",
			                    desc: "desc",
			                    image: "image",
			                    price: 11.1,
			                    specialPrice: 22.2,
			                    discountPercentage: 33.3,
			                    favorite: true)
		}
		
		describe("Creation") {
			it("shoud return the expected object after creating it") {
				
				expect(sut.sku).to(match("sku"))
				expect(sut.name).to(match("name"))
				expect(sut.desc).to(match("desc"))
				expect(sut.image).to(match("image"))
				expect(sut.price).to(equal(11.1))
				expect(sut.specialPrice).to(equal(22.2))
				expect(sut.discountPercentage).to(equal(33.3))
				expect(sut.favorite).to(equal(true))
			}
		}
	}
}
