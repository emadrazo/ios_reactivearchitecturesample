import Foundation
import lmCore

extension ProductExtendedEntity: JSONDecodable {
    func fill(dictionary: JSONDictionary) throws {

        let categoryId=""
        let subcategoryId=""
        let familyId=""
        let favorite = false

        guard let sku = dictionary["sku"] as? String else {
            throw JSONDecodingError.wrongJSONFormat
        }
        guard let name = dictionary["name"] as? String else {
            throw JSONDecodingError.wrongJSONFormat
        }
        guard let longDescription = dictionary["longDescription"] as? [String:Any],
            let desc = longDescription["text"] as? String else {
                throw JSONDecodingError.wrongJSONFormat
        }
        guard let images = dictionary["images"] as? [String],
            let image = images.first else {
                throw JSONDecodingError.wrongJSONFormat
        }
        guard let priceInfo = dictionary["priceInfo"] as? [String: Any],
            let price = priceInfo["price"] as? Float else {
                throw JSONDecodingError.wrongJSONFormat
        }
        guard let specialPrice = priceInfo["specialPrice"] as? Float else {
            throw JSONDecodingError.wrongJSONFormat
        }
        guard let discountPercentage = priceInfo["discountPercentage"] as? Float else {
            throw JSONDecodingError.wrongJSONFormat
        }

        self.sku = sku
        self.categoryId = categoryId
        self.subcategoryId = subcategoryId
        self.familyId = familyId
        self.name = name
        self.desc = desc
        self.image = image
        self.price = price
        self.specialPrice = specialPrice
        self.discountPercentage = discountPercentage
        self.favorite = favorite
    }
}
