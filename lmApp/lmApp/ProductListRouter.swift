import UIKit
import lmCore
import lmProduct

protocol ProductListRouterType {
	func didFinish()
	func navigateToDetail(product: ProductDetailEntityType)
}

class ProductListRouter: Router, ProductListRouterType {
	
	override func start() {
        
		let dbClient = DataBaseClient<ProductDetailEntity>()
		let productDB = ProductDataBaseRepository(dataBaseClient: dbClient)
		let apiClient = ProductApiClient<ProductDetailEntity>(apiClient: ApiClient<ProductDetailEntity>.shared)
		let productApiRepository = ProductApiRepository<ProductDetailEntity>(productApiClient: apiClient)
		let productModel = ProductModel(database: productDB, api: productApiRepository)
		let productService = ProductService(productModel: productModel)
        let getProductsWithParametersUseCase = GetProductsWithParametersUseCase(productService: productService)
		let viewModel = ProductListViewModel(router: self,
		                                     getProductsWithParametersUseCase: getProductsWithParametersUseCase)
		
        let productListViewController = ProductListViewController(viewModel: viewModel)
		
        navigationController?.pushViewController(productListViewController, animated: true)
    }
	
	public func navigateToDetail(product: ProductDetailEntityType) {
		ProductDetailRouter(navigationController: self.navigationController, product: product).start()
    }
}
