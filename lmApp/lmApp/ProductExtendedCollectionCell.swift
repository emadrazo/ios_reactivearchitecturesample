
import UIKit
import lmCore
import AlamofireImage
import RxSwift

class ProductExtendedCollectionCell: UICollectionViewCell {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productFavouriteButton: UIButton!

    fileprivate var viewModel: ProductExtendedCollectionCellViewModelType?

    private var bag = DisposeBag()

    override func prepareForReuse() {
        productImage.image = nil
        productNameLabel.text=""
        productPriceLabel.text=""
        productFavouriteButton.isSelected=false

        bag = DisposeBag()
    }

    func configure(product:ProductDetailEntity, viewModel: ProductExtendedCollectionCellViewModelType) {
        self.viewModel = viewModel
        self.setupProduct(product)
        self.setupRx()

        //viewModel.reloadProduct()
    }

    func setupProduct(_ product:ProductDetailEntity){
        if let url = URL(string: product.image) {
            self.productImage?.af_setImage(withURL: url)
        }

        productNameLabel.text = product.name
        productPriceLabel.text = "\(product.price)"   //TODO Formatter comun
        productFavouriteButton.isSelected = product.favorite

    }
    func setupRx() {

        /*guard let viewModel = viewModel else {
            return
        }

        viewModel.productName
            .bind(to: self.productNameLabel.rx.text)
            .addDisposableTo(self.bag)

        viewModel.productImage.subscribe(onNext: {
            if let url = URL(string: $0) {
                self.productImage?.af_setImage(withURL: url)
            }
        }).addDisposableTo(self.bag)


        viewModel.productIsFavourite
            .bind(to: self.productFavouriteButton.rx.isSelected)
            .addDisposableTo(self.bag)
         */
        self.productFavouriteButton.rx.controlEvent(UIControlEvents.touchUpInside).subscribe(
            onNext: {[weak self] event in
                guard let strongSelf = self else  { return }
                strongSelf.viewModel?.setFavourite()
            
        }, onError: nil, onCompleted: {}, onDisposed: {}).addDisposableTo(self.bag)
    }
}
