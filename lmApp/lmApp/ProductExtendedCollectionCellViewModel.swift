import RxSwift
import lmProduct
import lmCore

protocol ProductExtendedCollectionCellViewModelType {
    var productName: Observable<String> {get}
    var productImage: Observable<String> {get}
    var productIsFavourite: Observable<Bool> {get}
    var productIsPrice: Observable<String> {get}

    func setFavourite()
}

final class ProductExtendedCollectionCellViewModel:  ProductExtendedCollectionCellViewModelType{

    // output
    private var productNameVariable: Variable<String> = Variable("")
    private var productImageVariable: Variable<String> = Variable("")
    private var productIsFavoriteVariable: Variable<Bool> = Variable(false)
    private var productPriceVariable: Variable<String> = Variable("")
    fileprivate var product = Variable(ProductDetailEntity())

    lazy var productName: Observable<String> = self.productNameVariable.asObservable()
    lazy var productImage: Observable<String> = self.productImageVariable.asObservable()
    lazy var productIsFavourite: Observable<Bool> = self.productIsFavoriteVariable.asObservable()
    lazy var productIsPrice: Observable<String> = self.productPriceVariable.asObservable()

    // private
    private let bag = DisposeBag()
    private let router: HomeRouterType
    private let makeFavouriteProductUseCase: SetFavoriteProductUseCase<ProductDetailEntity,UserEntity>
    /*private let getProductUseCase: GetProductDetailUseCase<ProductDetailEntity>

    init(router:HomeRouterType, getProductUseCase: GetProductDetailUseCase<ProductDetailEntity>, makeFavouriteProductUseCase: SetFavoriteProductUseCase<ProductDetailEntity,UserEntity>) {
        self.router = router
        self.makeFavouriteProductUseCase = makeFavouriteProductUseCase
        self.getProductUseCase = getProductUseCase

        self.setupRx()
    }*/

    init(router:HomeRouterType, makeFavouriteProductUseCase: SetFavoriteProductUseCase<ProductDetailEntity,UserEntity>) {
        self.router = router
        self.makeFavouriteProductUseCase = makeFavouriteProductUseCase

        self.setupRx()
    }

    // MARK: Setup

    private func setupRx() {
        product.asObservable()
            .subscribe(onNext: { [weak self] product in
                guard let strongSelf = self else  { return }
                strongSelf.productNameVariable.value = product.name
                strongSelf.productImageVariable.value = product.image
                strongSelf.productIsFavoriteVariable.value = product.favorite
                //TODO Utils formater strongSelf.productPriceVariable.value = product.price
            })
            .addDisposableTo(self.bag)
    }


    /*func reloadProduct() {
        getProductUseCase.execute()
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] transaction in
                guard let strongSelf = self else  { return }
                if transaction.isSuccess {
                    guard let product = transaction.value else {
                        return
                    }
                    strongSelf.product.value = product

                } else if transaction.isError {
                    guard let error = transaction.error as? ApiError else {
                        return
                    }
                    switch error {
                    case .networkException: break
                    //strongSelf.errorMesssageVariable.value = "No funsiona intenneeee!"
                    case .timeOut: break
                    //strongSelf.errorMesssageVariable.value = "Sacabó!"
                    default: break
                        //strongSelf.errorMesssageVariable.value = "Sehodió!"
                    }
                }
            })
            .addDisposableTo(bag)
    }
*/

    func setFavourite(){
        self.makeFavouriteProductUseCase.execute()
            .subscribe(onNext: { [weak self] transaction in
                guard let strongSelf = self else  { return }
                switch transaction {
                case .success(let product):
                    strongSelf.productIsFavoriteVariable.value = product.favorite
                case .error(let error):
                    switch error {
                    case SetFavoriteProductUseCaseError.userIsNotLogged:
                        strongSelf.router.navigateToLogin()
                    default:
                        break
                    }
                }
            })
            .addDisposableTo(self.bag)
    }
}



