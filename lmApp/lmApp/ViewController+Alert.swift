import UIKit

extension UIViewController {

    func alert(title: String, text: String?) {
        let alertVC = UIAlertController(title: title, message: text, preferredStyle: .alert)
        alertVC.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertVC, animated: true, completion: nil)
    }
	
	func embedInNavigation() -> UINavigationController {
		return UINavigationController.init(rootViewController: self)
	}
	
	@objc func close() {
		if presentingViewController != nil {
			dismiss(animated: true, completion: nil)
		} else {
			self.navigationController?.popViewController(animated: true)
		}
	}
}
