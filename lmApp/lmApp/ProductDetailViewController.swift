import UIKit
import RxSwift
import RxCocoa

class ProductDetailViewController: UIViewController {
    
	fileprivate let bag = DisposeBag()

	@IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
	@IBOutlet weak var favoritoSwitch: UISwitch!
    
    fileprivate let viewModel: ProductDetailViewModelType
	
	// MARK: Init

	init(viewModel: ProductDetailViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
	
	// MARK: Life cycle

	override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupLayout()
        setupRx()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		viewModel.reloadDetail()
	}
	
    // MARK: Setup
	
	func setupViews() {
        
    }
    
    func setupLayout() {
        
    }
    
    func setupRx() {
		
		favoritoSwitch.rx.controlEvent(.valueChanged)
			.bind(onNext: self.viewModel.favoriteDidChange)
			.addDisposableTo(self.bag)
		
        viewModel.productName
            .bind(to: self.productNameLabel.rx.text)
            .addDisposableTo(self.bag)
		
        viewModel.productDescription
            .bind(to: self.productDescriptionLabel.rx.text)
            .addDisposableTo(self.bag)
		
		viewModel.productIsFavorite
			.bind(to: self.favoritoSwitch.rx.isOn)
			.addDisposableTo(self.bag)
		
        viewModel.errorMesssage
            .bind(to: self.errorLabel.rx.text)
            .addDisposableTo(self.bag)
    }
}
