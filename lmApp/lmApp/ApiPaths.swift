import Foundation

public enum ApiPaths: String {
    
    case product
    case products
    case category
    case subcategory
    case family
}
