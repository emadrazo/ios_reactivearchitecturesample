import Foundation
import RealmSwift

public typealias JSONDictionary = [String: Any]

public protocol JSONDecodable {
    init()
    func fill(dictionary: JSONDictionary) throws
}


extension JSONDecodable {
    init(dictionary: JSONDictionary) throws{
        self.init()
        try self.fill(dictionary: dictionary)
    }
}

//TODO: - Deberian ir estas funciones en la app??
public func decode<T: JSONDecodable>(_ dictionary: JSONDictionary) -> T? {
    //TODO: - hacer un do catch!!!!
    return try? T(dictionary: dictionary)
}

public func decode<T: JSONDecodable>(_ dictionaries: [JSONDictionary]) -> [T] {
    return dictionaries.flatMap(decode)
}

/*public func decode<T: JSONDecodable>(_ data: Data) -> T? {
    guard
        let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []),
        let jsonDictionary = jsonObject as? JSONDictionary
        else {
            return nil
        }


    return try T(dictionary: jsonDictionary)
}*/

/*


protocol JSONDecodable: AnyObject {
    func decode(dictionary: JSONDictionary) throws -> Self

}

extension Object: JSONDecodable {
    func decode(dictionary: JSONDictionary) throws -> Self {
        let me = type(of: self).init()

        return try me.decode(dictionary: dictionary)
    }

    func decode(_ data: Data) throws -> Self? {
        let me = type(of: self).init()
        guard
            let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []),
            let jsonDictionary = jsonObject as? JSONDictionary

            else {
                return nil
            }
        
        return try me.decode(dictionary: jsonDictionary)
    }
}





extension List.Element: JSONDecodable {
    func decode(dictionary:JSONDictionary) throws -> List<Element> {
        let result = List<Element>()
        try forEach {
            
            try result.append($0.decode(dictionary: dictionary[$0]))
        }
        return result
    }

    func decode(dictionary: [JSONDictionary]) -> [T] {
        return dictionaries.flatMap(type(of: self).init().decode($0))
    }
    
}

*/
