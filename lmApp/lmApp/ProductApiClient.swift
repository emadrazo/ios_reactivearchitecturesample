import lmProduct
import lmCore
import RxSwift

public class ProductApiClient<T>: ProductApiClientFor<T>  where T:ProductDetailEntityType, T:JSONDecodable{
    
    private let apiClient: ApiClient<T>
    private let baseURL: URL = URL(string: "https://private-fc907-products95.apiary-mock.com")!
        
    public init(apiClient: ApiClient<T>) {
        self.apiClient = apiClient
        super.init()
    }
    
    public override func getProductDetail(parameters: [String: String]) -> Observable<Transaction<T>> {
        
        let url = baseURL.appendingPathComponent(ApiPaths.product.rawValue)
        
        let resource = Resource(url: url, method: .get, parameters: parameters)
        
        return apiClient.getEntity(forResource: resource)
    }
    
    public override func getProducts(withParameters parameters: [String: Any]) -> Observable<Transaction<[T]>> {
        
        let category = parameters["categoryId"] as? String ?? ""
        let subcategory = parameters["subcategoryId"] as? String ?? ""
        let family = parameters["familyId"] as? String ?? ""

        var url = baseURL.appendingPathComponent(ApiPaths.product.rawValue)
        url.appendPathComponent(ApiPaths.category.rawValue)
        url.appendPathComponent(category)
        url.appendPathComponent(ApiPaths.subcategory.rawValue)
        url.appendPathComponent(subcategory)
        url.appendPathComponent(ApiPaths.family.rawValue)
        url.appendPathComponent(family)
        url.appendPathComponent(ApiPaths.products.rawValue)

        let isVariable = parameters["isVariable"] as? Bool ?? false
        let orderAscendant = parameters["orderAscendant"] as? Bool ?? false
        
        var parametersDict = ParametersDictionary()
        parametersDict["page"] = parameters["page"] as? String ?? ""
        parametersDict["rows"] = parameters["rows"] as? String ?? ""
        parametersDict["isVariable"] = isVariable ? "true" : "false"
        parametersDict["critreria"] = parameters["criteria"] as? String ?? ""
        parametersDict["orderBy"] = parameters["orderBy"] as? String ?? ""
        parametersDict["orderAscendant"] = orderAscendant ? "true":"false"
        
        let resource = Resource(url: url, method: .get, parameters: parametersDict)
        
        return apiClient.getEntitiyList(forResource: resource)
    }
}
