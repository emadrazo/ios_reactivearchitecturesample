import RxSwift
import lmProduct
import lmCore

protocol HomeViewModelType {
	var recommendedProductsObservable: Observable<[ProductDetailEntity]> {get}
	var errorObservable: Observable<Error> { get }
	
	func reloadHome()
	func didSelectProduct(_ product: ProductDetailEntity)

    func getViewModelForCell(indexPath: IndexPath) -> ProductExtendedCollectionCellViewModelType
}

class HomeViewModel:  HomeViewModelType{

	
	// output
	private var recommendedProductsVariable: Variable<[ProductDetailEntity]> = Variable([])
	private var errorSubject: PublishSubject<Error> = PublishSubject()
	
	lazy var recommendedProductsObservable: Observable<[ProductDetailEntity]> = self.recommendedProductsVariable.asObservable()
	lazy var errorObservable: Observable<Error> = self.errorSubject.asObservable()
	
	// private
	private let bag = DisposeBag()
	private let router: HomeRouterType
	private let gethomeUseCase: GetHomeUseCase<HomeEntity>
	
	init(router: HomeRouterType, useCase: GetHomeUseCase<HomeEntity>) {
		self.router = router
		self.gethomeUseCase = useCase
	}
	
	deinit {
		router.didFinish()
	}
	
	// MARK: HomeViewModelType
	
	func reloadHome() {
		gethomeUseCase.execute()
			.subscribe(onNext: { [weak self] transaction in
				guard let strongSelf = self else  { return }
				switch transaction {
				case .success(let home):
					strongSelf.recommendedProductsVariable.value = home.recommendedProducts!
				case .error(let error):
					strongSelf.errorSubject.onNext(error)
				}
			})
			.addDisposableTo(self.bag)
	}
	
	// MARK: Utils
	
	public func didSelectProduct(_ product: ProductDetailEntity) {
		self.router.navigateToProductDetail(product: product)
	}


    func getViewModelForCell(indexPath: IndexPath) -> ProductExtendedCollectionCellViewModelType{

//        let product = self.recommendedProductsVariable.value[indexPath.row]
//
//        //TODO cambiar con la injección de dependencias
//        let productService = ProductService(
//            productModel: ProductModel(
//                database: ProductDataBaseRepository(
//                    dataBaseClient: DataBaseClient<ProductDetailEntity>()), api: ProductApiRepository(
//                        productApiClient: ProductApiClient(
//                            apiClient: ApiClient<ProductDetailEntity>()))))
//
//        //TODO cambiar cuando product tenga todas las características
//        let params = GetProductDetailParameters(sku: product.sku, categoryId: "1", subcategoryId: "1", familyId: "1")
//
//        let getProductDetailUseCase = GetProductDetailUseCase(productService: productService, parameters: params)

        //return ProductExtendedCollectionCellViewModel(router: self.router, getProductUseCase: getProductDetailUseCase, makeFavouriteProductUseCase: SetFavoriteProductUseCase<ProductDetailEntity,UserEntity>())

        return ProductExtendedCollectionCellViewModel(router: self.router, makeFavouriteProductUseCase: SetFavoriteProductUseCase<ProductDetailEntity,UserEntity>())

    }



}

