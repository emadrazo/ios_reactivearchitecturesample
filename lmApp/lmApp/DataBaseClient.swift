import RealmSwift
import RxSwift
import lmCore

enum RealmError: Error {
    case error
}

/// Represents a Query
public protocol QueryType {
    var predicate: NSPredicate? { get }
    var sortDescriptors: [SortDescriptor] { get }
}

public enum DefaultQuery<V>: QueryType {
    case all
    case byKey(String, V)

    public var predicate: NSPredicate? {
        switch self {
        case .all:
            return nil
        case .byKey(let key, let value):
            if let value = value as? CVarArg {
                return NSPredicate(format: "\(key) == %@", value)
            } else {
                return nil
            }
        }
    }

    public var sortDescriptors: [SortDescriptor] {
        return []
    }
}

public typealias PropertyValuePair = (name: String, value: Any)

/// Represents a property value
public protocol PropertyValueType {
    var propertyValuePair: PropertyValuePair { get }
}

public enum DefaultPropertyValue: PropertyValueType {

    public var propertyValuePair: PropertyValuePair {
        return ("", 0)
    }
}

// LMDataBaseClient siempre devuelve objetos unmanaged. Esto implica que los cambios no se meten en BD hasta que no se llama a algún método de modificación de LMDataBaseClient
public class DataBaseClient<T>:DataBaseClientFor<T> where T:EntityType, T:Object {

    //private let realm: Realm
    private var realm: Realm {
        get {
            do {
                let realm = try Realm()
                return realm
            }
            catch {
                print("Could not access database: ", error)
            }
            return self.realm
        }
    }

    public  override init() {
    }

    public func values<U: Object> (_ type: U.Type, matching query: QueryType) -> Results<U> {
        var results = realm.objects(U.self)

        if let predicate = query.predicate {
            results = results.filter(predicate)
        }

        results = results.sorted(by: query.sortDescriptors)

        return results
    }

    public override func findFirst() -> Observable<Transaction<T>> {
        return Observable.create { [weak self] observer in

            if let results = self?.values(T.self, matching: DefaultQuery<T>.all), !results.isEmpty {
                observer.onNext(Transaction.success(results[0].detached()))
			}
            observer.onCompleted()
            return Disposables.create()
        }
    }

    public override func findAll() -> Observable<Transaction<[T]>> {
        return Observable.create { [weak self] observer in

            if let results = self?.values(T.self, matching: DefaultQuery<T>.all), !results.isEmpty {
                observer.onNext(Transaction.success(results.map { $0.detached() }))
			}
            observer.onCompleted()
            return Disposables.create()
        }
    }

    public override func findFirstEqualTo<V>(key: String, value: V)->Observable<Transaction<T>> {

        return Observable.create { [weak self] observer in

            if let results = self?.values(T.self, matching: DefaultQuery.byKey(key, value)), !results.isEmpty {
                observer.onNext(Transaction.success(results[0].detached()))
			}
            observer.onCompleted()
            return Disposables.create()
        }
    }

    public override func createOrUpdate (element: T) -> Observable<Transaction<T>> {
        return Observable.create { [weak self] observer in

            let attachedElement = element.detached()

            guard let realm = self?.realm else {
                observer.onNext(Transaction.error(RealmError.error))
                observer.onCompleted()
                return Disposables.create()
            }

            do {
                try realm.write {
                    realm.add(attachedElement, update: true)
                }
                observer.onNext(Transaction.success(element))
                
            } catch {
                observer.onNext(Transaction.error(RealmError.error))
            }

            observer.onCompleted()
            return Disposables.create()
        }
    }

    public override func removeFirst <V>(key: String, value: V) -> Observable<Transaction<T>> {

        return Observable.create { [weak self] observer in
            guard let realm = self?.realm else {
                observer.onNext(Transaction.error(RealmError.error))
                observer.onCompleted()
                return Disposables.create()
            }

            if let results = self?.values(T.self, matching: DefaultQuery.byKey(key, value)), !results.isEmpty {
                do{
                    try realm.write {
                        realm.delete(results[0])
                    }
                } catch {
                    observer.onNext(Transaction.error(RealmError.error))
				}
            }

            observer.onCompleted()
            return Disposables.create()
            }.catchError { error in
                return Observable.just(Transaction<T>.error(RealmError.error))
        }
    }

    public override func removeAll() -> Observable<Transaction<T>> {
        return Observable.create { [weak self] observer in

            guard let realm = self?.realm else {
                observer.onNext(Transaction.error(RealmError.error))
                observer.onCompleted()
                return Disposables.create()
            }

            if let results = self?.values(T.self, matching: DefaultQuery<T>.all), !results.isEmpty {
                do{
                    try realm.write {
                        realm.delete(results)
                    }
                } catch {
                    observer.onNext(Transaction.error(RealmError.error))
				}
            }
			
            observer.onCompleted()
            return Disposables.create()
            }.catchError { error in
                return Observable.just(Transaction<T>.error(RealmError.error))
        }
    }
}
