import UIKit
import lmCommerce

protocol HomeRouterType {
    func didFinish()
    func navigateToProductDetail(product: ProductDetailEntity)
    func navigateToCategoryList()
    func navigateToProductList() //Promociones, destacados..
    func navigateToScanner()
    func navigateToLogin()
}

class HomeRouter: Router, HomeRouterType {

    override func start() {

        let product0:ProductDetailEntity = ProductDetailEntity(sku: "CBX54A",
                                                   categoryId: "1",
                                                   subcategoryId: "1",
                                                   familyId: "1",

                                                   name: "Test product 1",
                                                   desc: "A product for testing purposes",
                                                   image: "https://www.leroymerlin.es/img/r25/44/4410/441007/19730480/19730480_f1.jpg",
                                                   price: 1.27,
                                                   specialPrice: 1.27,
                                                   discountPercentage: 0.0,
                                                   favorite: true)
        
        let product1:ProductDetailEntity = ProductDetailEntity(sku: "AAX54A",
                                                   categoryId: "1",
                                                   subcategoryId: "1",
                                                   familyId: "1",
                                                   name: "Test product 2",
                                                   desc: "A product for testing purposes",
                                                   image: "https://www.leroymerlin.es/img/r25/60/6003/600306/panel_ducha_solar_espejo_2_hojas/17052595_f1.jpg",
                                                   price: 5.00,
                                                   specialPrice: 2.50,
                                                   discountPercentage: 50.0,
                                                   favorite: false)

        let product2:ProductDetailEntity = ProductDetailEntity(sku: "54ABB",
                                                   categoryId: "1",
                                                   subcategoryId: "1",
                                                   familyId: "1",
                                                   name: "Test product 3",
                                                   desc: "A product for testing purposes",
                                                   image: "image 3",
                                                   price: 1.27,
                                                   specialPrice: 1.27,
                                                   discountPercentage: 0.0,
                                                   favorite: false)

        let product3:ProductDetailEntity = ProductDetailEntity(sku: "4ATNB",
                                                   categoryId: "1",
                                                   subcategoryId: "1",
                                                   familyId: "1",
                                                   name: "Test product 4",
                                                   desc: "A product for testing purposes",
                                                   image: "image 4",
                                                   price: 5.00,
                                                   specialPrice: 2.50,
                                                   discountPercentage: 50.0,
                                                   favorite: false)

        let product4:ProductDetailEntity = ProductDetailEntity(sku: "B5A33",
                                                   categoryId: "1",
                                                   subcategoryId: "1",
                                                   familyId: "1",
                                                   name: "Test product 5",
                                                   desc: "A product for testing purposes",
                                                   image: "image 5",
                                                   price: 1.27,
                                                   specialPrice: 1.27,
                                                   discountPercentage: 0.0,
                                                   favorite: false)

        let product5:ProductDetailEntity = ProductDetailEntity(sku: "ABBC32",
                                                   categoryId: "1",
                                                   subcategoryId: "1",
                                                   familyId: "1",
                                                   name: "Test product 6",
                                                   desc: "A product for testing purposes",
                                                   image: "image 6",
                                                   price: 5.00,
                                                   specialPrice: 2.50,
                                                   discountPercentage: 50.0,
                                                   favorite: false)

        let dummyHome = HomeEntity(name: "Home", recommendedProducts: [product0, product1, product2, product3,product4, product5], viewedProducts: [])

        let apiClient = LMHomeApiClient<HomeEntity>(apiClient: ApiClient<HomeEntity>.shared)
        apiClient.dummyHome(home: dummyHome)
        let homeAPI = HomeApiRepository<HomeEntity>(homeApiClient: apiClient)

        let homeModel = HomeModel(api: homeAPI)

        let homeService = HomeService(homeModel: homeModel)

        let homeUseCase = GetHomeUseCase(homeService: homeService)

        let viewModel = HomeViewModel(router:self, useCase: homeUseCase)
        let viewController = HomeViewController(viewModel: viewModel)

        navigationController?.pushViewController(viewController, animated: true)
    }

    func navigateToProductDetail(product: ProductDetailEntity) {
        ProductDetailRouter(navigationController: self.navigationController, product: product).start()
    }
    
    func navigateToCategoryList() {}
    
    func navigateToProductList() {}
    
    func navigateToScanner() {}
    
    func navigateToLogin() {
        print("navigating to login")
    }
}
