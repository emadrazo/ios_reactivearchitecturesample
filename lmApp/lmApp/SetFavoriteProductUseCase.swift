import lmCommerce
import lmCore
import lmProduct
import RxSwift
import RealmSwift

import Foundation

public enum SetFavoriteProductUseCaseError: Error {
    case userIsNotLogged
    case productDoesNotExist
}

final class SetFavoriteProductUseCase<T,U> where T:ProductDetailEntityType, U:UserEntityType, T:Object, T:JSONDecodable {

    let productService: ProductService<T>
    let userService: UserService<U>

    init(productService: ProductService<T> = ProductService(
        productModel: ProductModel(
            database: ProductDataBaseRepository(
                dataBaseClient: DataBaseClient<T>()),
            api: ProductApiRepository(productApiClient: ProductApiClient(apiClient: ApiClient<T>.shared))))

        , userService: UserService<U> = UserService() ) {
        self.productService = productService
        self.userService = userService
    }

    func execute() -> Observable<Transaction<T>> {
        return Observable.just(Transaction.error(SetFavoriteProductUseCaseError.userIsNotLogged))
    }
}


//TODO QUITAR
open class UserService<T> where T:UserEntityType {
    required public init() {
        
    }
}
