import lmProduct
import lmCore
import RxSwift

class GetProductsWithParametersUseCase<T> where T:ProductDetailEntityType {
    
    let service: ProductService<T>
    
    init(productService: ProductService<T>) {
        self.service = productService
    }
    
    func execute(withParameters parameters: GetProductsParameters) -> Observable<Transaction<[T]>> {
        return service.getProducts(withParameters: parameters)
    }
}
