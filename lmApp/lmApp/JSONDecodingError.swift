import Foundation

enum JSONDecodingError : Error {
    case wrongJSONFormat
    case wrongURLFormat
    case errorDecodingJSON
    case nilJSONObject
}
