import UIKit
import lmCore
import lmProduct

protocol ProductDetailRouterType {
    func didFinish()
}

class ProductDetailRouter: Router, ProductDetailRouterType {
    
    let product: ProductDetailEntityType
    
    init(navigationController: UINavigationController?, product: ProductDetailEntityType) {
        self.product = product
        super.init(navigationController: navigationController)
    }
    
    override func start() {
        
        let dbClient = DataBaseClient<ProductDetailEntity>()
        let productDB = ProductDataBaseRepository(dataBaseClient: dbClient)
        let apiClient = ProductApiClient<ProductDetailEntity>(apiClient: ApiClient<ProductDetailEntity>.shared)
        let productApi = ProductApiRepository<ProductDetailEntity>(productApiClient: apiClient)
        let productModel = ProductModel(database: productDB, api: productApi)
        let productService = ProductService(productModel: productModel)
        let getProductUseCase = GetProductDetailUseCase(productService: productService)
        let updateProductUseCase = UpdateProductUseCase(productService: productService)		
		let viewModel = ProductDetailViewModel(router: self,
		                                       getProductUseCase: getProductUseCase,
		                                       updateProductUseCase: updateProductUseCase,
		                                       parameters: GetProductDetailParameters(sku: product.sku,
		                                                                              categoryId: "1",
		                                                                              subcategoryId: "1",
		                                                                              familyId: "1"))
		
        let viewController = ProductDetailViewController(viewModel: viewModel)
		
        navigationController?.pushViewController(viewController, animated: true)
    }
}
