import Foundation
import RealmSwift
import Realm
import lmCore

class ProductBaseEntity: Object, ProductBaseEntityType {

    dynamic var sku: String = ""
    dynamic var categoryId: String = ""
    dynamic var subcategoryId: String = ""
    dynamic var familyId: String = ""
    dynamic var name: String = ""
    dynamic var image: String = ""

    override public static func primaryKey() -> String? {
        return "sku"
    }

    //TODO habría que encontrar la manera de no tener que definir los 3 inits
    public required init() {
        super.init()
    }

    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }

    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }

    convenience required init(sku: String, categoryId: String, subcategoryId: String, familyId: String, name: String, image: String) {
        self.init()
        self.sku = sku
        self.categoryId = categoryId
        self.subcategoryId = subcategoryId
        self.familyId = familyId
        self.name = name
        self.image = image
    }

    public enum PropertyValue: PropertyValueType {
        case sku(String)
        case name(String)

        public var propertyValuePair: PropertyValuePair {
            switch self {
            case .sku(let sku):
                return ("sku", sku)
            case .name(let name):
                return ("name", name)
            }
        }
    }

    public enum Query: QueryType {
        case all
        case bySKU(String)
        case favorite(Bool)

        public var predicate: NSPredicate? {
            switch self {
            case .all:
                return nil
            case .bySKU(let value):
                return NSPredicate(format: "sku == %@", value)
            case .favorite(let value):
                return NSPredicate(format: "favorite == %@", NSNumber.init(value: value))
            }
        }
        
        public var sortDescriptors: [SortDescriptor] {
            return [SortDescriptor(keyPath: "name")]
        }
    }
}
