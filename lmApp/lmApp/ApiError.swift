import Foundation

public enum ApiError: Error {
    case networkException
    case timeOut
    case notFound
    case couldNotDecodeJSON
}
