import Foundation
import RealmSwift
import lmCore

final class HomeEntity: /*Object, */HomeEntityType {
    typealias Product = ProductDetailEntity

    dynamic var name: String = ""
    dynamic var recommendedProducts: [Product]? = []
    var viewedProducts: [Product]? = []

    //var viewedProducts = List<Product>()

    /*override public static func primaryKey() -> String? {
        return "name"
    }*/

    public convenience init(name: String, recommendedProducts: [Product]?, viewedProducts: [Product]?){
        self.init()
        self.name = name
        self.recommendedProducts=recommendedProducts
        self.viewedProducts=viewedProducts
    }
}
