import Foundation
import RealmSwift
import lmCore

final class UserEntity: Object, UserEntityType {
    typealias Product = ProductDetailEntity

    dynamic var identifier: String = ""
    dynamic var name: String = ""

    override public static func primaryKey() -> String? {
        return "identifier"
    }

    public convenience init(identifier: String, name: String){
        self.init()
        self.identifier = identifier
        self.name = name
    }

    public enum PropertyValue: PropertyValueType {
        case identifier(String)
        case name(String)
        
        public var propertyValuePair: PropertyValuePair {
            switch self {
            case .identifier(let identifier):
                return ("identifier", identifier)
            case .name(let name):
                return ("name", name)
            }
        }
    }
    
    public enum Query: QueryType {
        case all
        case identifier(String)
		case name(String)
		
        public var predicate: NSPredicate? {
            switch self {
            case .all:
                return nil
            case .identifier(let value):
                return NSPredicate(format: "identifier == %@", value)
			case .name(let value):
				return NSPredicate(format: "name == %@", value)
            }
        }
        
        public var sortDescriptors: [SortDescriptor] {
            return [SortDescriptor(keyPath: "name")]
        }
    }
}
