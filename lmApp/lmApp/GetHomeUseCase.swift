import lmCommerce
import lmCore
import RxSwift

import Foundation

class GetHomeUseCase<T> where T:HomeEntityType {

    let service: HomeService<T>

    init(homeService: HomeService<T>) {
        self.service = homeService
    }

    func execute() -> Observable<Transaction<T>> {
        return service.getHome()
    }
}
