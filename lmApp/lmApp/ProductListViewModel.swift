import RxSwift
import lmProduct
import lmCore

protocol ProductListViewModelType {
	//outputs
	var productsObservable: Observable<[ProductDetailEntity]> { get }
	var errorObservable: Observable<Error> { get }

	func realoadList()
	func productSelected(_ product: ProductDetailEntityType)
}

class ProductListViewModel: ProductListViewModelType {
	//private
	fileprivate let bag = DisposeBag()
	private let router: ProductListRouterType
    private let getProductsWithParametersUseCase: GetProductsWithParametersUseCase<ProductDetailEntity>

	//output
	private var productsVariable: Variable<[ProductDetailEntity]> = Variable([])
	lazy var productsObservable: Observable<[ProductDetailEntity]> = self.productsVariable.asObservable()

	private var errorSubject = PublishSubject<Error>()
	lazy var errorObservable: Observable<Error> = self.errorSubject.asObservable()

	// MARK: Init

	init(router:ProductListRouterType, getProductsWithParametersUseCase: GetProductsWithParametersUseCase<ProductDetailEntity>) {
		self.router = router
        self.getProductsWithParametersUseCase = getProductsWithParametersUseCase
	}
	
	deinit {
		router.didFinish()
	}
	
	// MARK: ProductListViewModelType
	
    //TODO: - no funciona aun porque necesitamos el modelo de ProductExtended,
    public func realoadList() {
        
        let parameters = GetProductsParameters(categoryId: "1",
                                               subcategoryId: "1",
                                               familyId: "1",
                                               page: 1,
                                               rows: 20,
                                               isVariable: false,
                                               criteria: "",
                                               orderBy: "name",
                                               orderAscendant: true)
        
        getProductsWithParametersUseCase.execute(withParameters: parameters)
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] transaction in
                guard let strongSelf = self else  { return }
                switch transaction {
                case .success(let products):
                    strongSelf.productsVariable.value = products
                case .error(let error):
                    strongSelf.errorSubject.onNext(error)
                }
            })
            .addDisposableTo(self.bag)
    }
	
	// MARK: Utils
	
	public func productSelected(_ product: ProductDetailEntityType) {
		self.router.navigateToDetail(product: product)
	}
}
