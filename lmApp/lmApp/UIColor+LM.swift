import UIKit

extension UIColor {
    static var lmaVerdeLeroy: UIColor {
        return UIColor(red: 116.0 / 255.0, green: 198.0 / 255.0, blue: 96.0 / 255.0, alpha: 1.0)
    }
}
