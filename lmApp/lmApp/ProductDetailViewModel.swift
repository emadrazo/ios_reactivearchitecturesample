import RxSwift
import RxCocoa
import lmCore
import lmProduct

protocol ProductDetailViewModelType {

	var errorMesssage: Observable<String> { get }
	var productName: Observable<String> { get }
	var productDescription: Observable<String> { get }
	var productIsFavorite: Observable<Bool> { get }

	func reloadDetail()
	func favoriteDidChange()
}

class ProductDetailViewModel: ProductDetailViewModelType {
	//private
	fileprivate let bag = DisposeBag()
    fileprivate let router: ProductDetailRouterType
    fileprivate let getProductUseCase: GetProductDetailUseCase<ProductDetailEntity>
    fileprivate let updateProductUseCase: UpdateProductUseCase<ProductDetailEntity>

	fileprivate var product = Variable(ProductDetailEntity())
	fileprivate var parameters: GetProductDetailParameters
	
	//output
	private var errorMesssageVariable = Variable("")
	lazy var errorMesssage: Observable<String> = self.errorMesssageVariable.asObservable()

    private var productNameVariable = Variable("")
	lazy var productName: Observable<String> = self.productNameVariable.asObservable()
	
	private var productIsFavoriteVariable = Variable(false)
	lazy var productIsFavorite: Observable<Bool> = self.productIsFavoriteVariable.asObservable()

	private var productDescriptionVariable = Variable("")
	lazy var productDescription: Observable<String> = self.productDescriptionVariable.asObservable()
	
	// MARK: Init
	
	init(router:ProductDetailRouterType,
	     getProductUseCase: GetProductDetailUseCase<ProductDetailEntity>,
	     updateProductUseCase: UpdateProductUseCase<ProductDetailEntity>,
	     parameters: GetProductDetailParameters) {
		self.router = router
		self.getProductUseCase = getProductUseCase
		self.updateProductUseCase = updateProductUseCase
		self.parameters = parameters
		
		setupRx()
	}
	
	deinit {
		router.didFinish()
	}
	
	// MARK: Setup
	
	private func setupRx() {
		product.asObservable()
			.subscribe(onNext: { [weak self] product in
				guard let strongSelf = self else  { return }
                strongSelf.productNameVariable.value = product.name
				strongSelf.productDescriptionVariable.value = product.desc
				strongSelf.productIsFavoriteVariable.value = product.favorite
			})
			.addDisposableTo(self.bag)
	}
	
	// MARK: ProductDetailViewModelType
	
	public func reloadDetail() {
		getProductUseCase.execute(parameters: parameters)
			.subscribeOn(MainScheduler.instance)
			.subscribe(onNext: { [weak self] transaction in
				guard let strongSelf = self else  { return }
				if transaction.isSuccess {
					guard let product = transaction.value else {
						return
					}
					strongSelf.product.value = product
					
				} else if transaction.isError {
					guard let error = transaction.error as? ApiError else {
						return
					}
					switch error {
					case .networkException:
						strongSelf.errorMesssageVariable.value = "No funsiona intenneeee!"
					case .timeOut:
						strongSelf.errorMesssageVariable.value = "Sacabó!"
					default:
						strongSelf.errorMesssageVariable.value = "Sehodió!"
					}
				}
			})
			.addDisposableTo(bag)
	}
	
	public func favoriteDidChange() {
		product.value.favorite = !product.value.favorite
		updateProductUseCase.execute(withParameters: UpdateProductParameters(product: product.value))
			.subscribe(onNext: { [weak self] transaction in
				guard let strongSelf = self else  { return }
				if transaction.isError {
					strongSelf.errorMesssageVariable.value = transaction.error?.localizedDescription ?? ""
				}
			})
			.addDisposableTo(self.bag)
	}
}
