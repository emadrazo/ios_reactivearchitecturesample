import Foundation
import RealmSwift
import Realm
import lmCore

final class ProductDetailEntity: ProductExtendedEntity, ProductDetailEntityType {

    dynamic var longDesc: String = ""

    public required init() {
        super.init()
    }

    required init(value: Any, schema: RLMSchema) {
        super.init(value: value, schema: schema)
    }

    required init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }

    public convenience init(sku: String, categoryId:String, subcategoryId:String, familyId:String, name: String, desc: String, image: String, price: Float, specialPrice: Float, discountPercentage: Float, favorite: Bool, longDesc: String) {
        self.init(sku: sku, categoryId: categoryId, subcategoryId: subcategoryId, familyId: familyId, name: name, desc: desc, image: image, price: price, specialPrice: specialPrice, discountPercentage: discountPercentage, favorite: favorite, longDesc: longDesc)
    }
    
    public convenience required init(sku: String, categoryId:String, subcategoryId:String, familyId:String, name: String, desc: String, image: String, price: Float, specialPrice: Float, discountPercentage: Float, favorite: Bool){
        self.init(sku: sku, categoryId: categoryId, subcategoryId: subcategoryId, familyId: familyId, name: name, image: image)
        self.desc = desc
        self.price = price
        self.specialPrice = specialPrice
        self.discountPercentage = discountPercentage
        self.favorite = favorite
    }

    public convenience required init(sku: String, categoryId: String, subcategoryId: String, familyId: String, name: String, image: String) {
        self.init()
        self.sku = sku
        self.categoryId = categoryId
        self.subcategoryId = subcategoryId
        self.familyId = familyId
        self.name = name
        self.image = image
    }

}
