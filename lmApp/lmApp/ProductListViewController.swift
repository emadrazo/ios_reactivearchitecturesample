import UIKit
import RxSwift
import RxCocoa
import lmCore
import lmProduct

class ProductListViewController: UIViewController {
    
	fileprivate let bag = DisposeBag()

	fileprivate let viewModel: ProductListViewModelType

    private let refresh = UIRefreshControl()
    
	@IBOutlet private weak var tableView: UITableView! {
		didSet {
            if #available(iOS 10.0, *) {
                tableView.refreshControl = refresh
            } else {
                // Fallback on earlier versions
            }
			tableView.register(UINib.init(nibName: "ProductCell", bundle: nil), forCellReuseIdentifier: "ProductCell")
		}
	}
	
	// MARK: Init

	init(viewModel: ProductListViewModelType) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
	// MARK: Life cycle

	override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupLayout()
        setupRx()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		viewModel.realoadList()
	}
    
    // MARK: Setup
	
    func setupViews() {

	}
    
    func setupLayout() {
        
    }
    
    func setupRx() {
		
		viewModel.productsObservable
			.bind(to: self.tableView.rx.items(cellIdentifier: "ProductCell", cellType: UITableViewCell.self)) { _, product, cell in
				cell.textLabel?.text = product.name
				cell.detailTextLabel?.text = product.desc
			}
			.addDisposableTo(self.bag)
		
		viewModel.errorObservable
			.subscribe(onNext: { [weak self] error in
				guard let strongSelf = self else  { return }
				strongSelf.alert(title: "Error", text: error.localizedDescription)
			})
			.addDisposableTo(self.bag)
		
        tableView.rx
            .modelSelected(ProductDetailEntity.self)
            .subscribe(onNext: { [weak self] product in
				guard let strongSelf = self else  { return }
				strongSelf.viewModel.productSelected(product)
                
                if let selectedRow = strongSelf.tableView.indexPathForSelectedRow {
                    strongSelf.tableView.deselectRow(at: selectedRow, animated: true)
                }
            }).addDisposableTo(self.bag)
        
        refresh.rx.controlEvent(.valueChanged)
            .map { _ in !self.refresh.isRefreshing }
            .filter { $0 == false }
            .subscribe(onNext: { [weak self] _ in
				guard let strongSelf = self else  { return }
                strongSelf.viewModel.realoadList()
            })
			.addDisposableTo(self.bag)
        
        refresh.rx.controlEvent(.valueChanged)
            .map { _ in self.refresh.isRefreshing }
            .filter { $0 == true }
            .subscribe(onNext: { [weak self] _ in
				guard let strongSelf = self else  { return }
                strongSelf.refresh.endRefreshing()
            })
			.addDisposableTo(self.bag)
    }
}
