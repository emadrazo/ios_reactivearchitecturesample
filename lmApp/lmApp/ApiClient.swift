import lmCore
import Alamofire
import RxSwift

fileprivate var singletons_store = [String: Any]()

public final class ApiClient<T>: ApiClientFor<T> where T:EntityType, T:JSONDecodable {
    
    private override init() {
//         Configurar AlamoFire: headers, certificados, token...
        super.init()
        print("ApiClient Singleton of type \(self) created")
    }
    
    class var shared : ApiClient<T> {
        
        let store_key = String(describing: T.self)
        
        if let singleton = singletons_store[store_key] as? ApiClient<T> {
            return singleton 
        } else {
            let new_singleton = ApiClient<T>()
            singletons_store[store_key] = new_singleton
            return new_singleton
        }
    }

    public override func getEntity(forResource resource: Resource) -> Observable<Transaction<T>> {
        return request(forResource: resource)
            .map { transaction in
                if transaction.isSuccess {
                    guard let dataDict = transaction.value as? JSONDictionary else {
                            throw ApiError.couldNotDecodeJSON
                    }
                    guard let entity: T = self.result(dataDict) else {
                        throw ApiError.couldNotDecodeJSON
                    }
                    return Transaction.success(entity)
                } else {
                    return Transaction.error(ApiError.couldNotDecodeJSON)
                }
        }
    }
    
    public override func getEntitiyList(forResource resource: Resource) -> Observable<Transaction<[T]>>{
        return request(forResource: resource)
            .map { transaction in
                if transaction.isSuccess {
                    guard let dataDict = transaction.value as? JSONDictionary,
                        let entities: [T] = self.results(dataDict)else {
                            throw ApiError.couldNotDecodeJSON
                    }
                    return Transaction.success(entities)
                } else {
                    return Transaction.error(ApiError.couldNotDecodeJSON)
                }
        }
    }
    
    private func request(forResource resource: Resource) -> Observable<Transaction<JSONDictionary?>> {
        return Observable.create { observer in
            
            let headers: HTTPHeaders = [
                //        "Authorization": "Basic ",
                "Accept": "application/json",
                "Content-Type": "application/json"
            ]
            
            // TODO: - Añadir validacion a alamofire o filtro de Rx de HTTPStatus
            // sumar los parametros a los parametros por defecto....
            let request = Alamofire
                .request(resource.url,
                         method: HTTPMethod(rawValue: resource.method.rawValue) ?? .get,
                         parameters: resource.parameters,
                         encoding: URLEncoding.default,
                         headers: headers)
                .responseJSON(completionHandler: {response in
                    
                    guard response.result.isSuccess else {
                        observer.onError(response.error ?? ApiError.networkException)
                        return
                    }
                    observer.onNext(response.result.value as? JSONDictionary)
                    observer.onCompleted()
                    
                })
            return Disposables.create(with: {
                request.cancel()
            })
            }
            .flatMap { data in
                return Observable.just(Transaction.success(data))
            }
            .retry(3)
            .catchError { error in
                return Observable.just(Transaction.error(ApiError.networkException))
            }
            .timeout(5000,
                     other: Observable.just(Transaction.error(ApiError.timeOut)),
                     scheduler: MainScheduler.instance)
        
    }
    
    //MARK: - Utils
    
    private func result<T: JSONDecodable>(_ dict: Any) -> T? {
        return (dict as? JSONDictionary).flatMap(decode)
    }
    private func results<T: JSONDecodable>(_ dict: Any) -> [T]? {
        return (dict as? [JSONDictionary]).flatMap(decode)
    }
}
