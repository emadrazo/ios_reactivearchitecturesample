import lmCommerce
import lmCore
import RxSwift


final class LMHomeApiClient<T>: HomeApiClient<T>  where T:HomeEntityType, T:JSONDecodable{
    public typealias Ent = T

    fileprivate let apiClient: ApiClient<T>
    fileprivate var dummyHome: T?

    public required init(apiClient: ApiClient<T>) {
        self.apiClient = apiClient
        super.init()
    }

    public func dummyHome(home:T){
        self.dummyHome = home
    }

    open override func getHome() -> Observable<Transaction<T>>{

        guard let dummy = dummyHome else{
            return Observable.of(Transaction.error(ApiError.notFound))
        }

        return Observable.of(Transaction.success(dummy))




    }
}
