import Foundation
import UIKit

class MainRouter: Router {

    private var window: UIWindow

    init(window: UIWindow) {
		self.window = window
		super.init(navigationController: nil)
    }

    override func start() {
        let mainTab = MainTabBarController()

        mainTab.viewControllers = makeViewControllers()

        window.rootViewController = mainTab
        window.makeKeyAndVisible()
    }

    private func makeViewControllers() -> [UIViewController] {

        let homeNavigationController = UINavigationController()
        let homeRouter = HomeRouter(navigationController: homeNavigationController)
        homeRouter.start()

        let productNavigationController = UINavigationController()
        let productRouter = ProductListRouter(navigationController: productNavigationController)
        productRouter.start()

        let dummy3NavigationController = UINavigationController()
        let dummy4NavigationController = UINavigationController()
        let dummy5NavigationController = UINavigationController()

        homeNavigationController.tabBarItem = UITabBarItem(
            title: "PRODUCTOS",
            image: UIImage(named: "ico_home_off"),
            selectedImage: UIImage(named: "ico_home"))

        productNavigationController.tabBarItem = UITabBarItem(
            title: "TIENDAS",
            image: UIImage(named: "ico_stores_off"),
            selectedImage: UIImage(named: "ico_stores"))

        dummy3NavigationController.tabBarItem = UITabBarItem(
            title: "CARRITO",
            image: UIImage(named: "ico_checkout_off"),
            selectedImage: UIImage(named: "ico_checkout"))

        dummy4NavigationController.tabBarItem = UITabBarItem(
            title: "LISTAS",
            image: UIImage(named: "ico_lists_off"),
            selectedImage: UIImage(named: "ico_lists"))

        dummy5NavigationController.tabBarItem = UITabBarItem(
            title: "TU",
            image: UIImage(named: "ico_you_off"),
            selectedImage: UIImage(named: "ico_you"))

        return [homeNavigationController, productNavigationController, dummy3NavigationController, dummy4NavigationController, dummy5NavigationController]
    }
}
