import UIKit
import RxSwift
import RxCocoa

class HomeViewController: UIViewController {
	
	@IBOutlet private weak var recommendedProductsCollectionView: UICollectionView! {
		didSet {
			recommendedProductsCollectionView.register(UINib.init(nibName: "ProductExtendedCollectionCell", bundle: nil), forCellWithReuseIdentifier: "ProductExtendedCollectionCell")
			recommendedProductsCollectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
		}
	}
	//@IBOutlet private weak var categoriesCollectionView: UICollectionView!
	//@IBOutlet private weak var lastProductsCollectionView: UICollectionView!
	
	fileprivate let viewModel: HomeViewModelType
	
	fileprivate let bag = DisposeBag()
	
	init(viewModel: HomeViewModelType) {
		self.viewModel = viewModel
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		setupViews()
		setupLayout()
		setupRx()
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		viewModel.reloadHome()
	}
	
	// MARK: Setup
	
	func setupViews() {
		automaticallyAdjustsScrollViewInsets = false
	}
	
	func setupLayout() {
		
	}
	
	func setupRx() {

		viewModel.recommendedProductsObservable
			.bind(to: self.recommendedProductsCollectionView.rx.items(cellIdentifier: "ProductExtendedCollectionCell", cellType: UICollectionViewCell.self)) { row , product, cell in
				guard let cell = cell as? ProductExtendedCollectionCell else { return }

                let indexPath = IndexPath(item: row, section: 0)
                cell.configure(product: product, viewModel: self.viewModel.getViewModelForCell(indexPath:indexPath))
			}
			.addDisposableTo(self.bag)
		
		viewModel.errorObservable
			.subscribe(onNext: { [weak self] error in
				guard let strongSelf = self else  { return }
				strongSelf.alert(title: "Error", text: error.localizedDescription)
			})
			.addDisposableTo(self.bag)
		
		recommendedProductsCollectionView.rx
			.modelSelected(ProductDetailEntity.self)
			.subscribe(onNext: { [weak self] product in
				guard let strongSelf = self else  { return }

				strongSelf.viewModel.didSelectProduct(product)
				
				if let selectedItemIndexPaths = strongSelf.recommendedProductsCollectionView.indexPathsForSelectedItems, selectedItemIndexPaths.count > 0 {
					let selectedItemIndexPath = selectedItemIndexPaths[0]
					strongSelf.recommendedProductsCollectionView.deselectItem(at: selectedItemIndexPath, animated: true)
				}
			}).addDisposableTo(self.bag)
	}
}
