import UIKit
import RxSwift
import RxCocoa

class MainTabBarController: UITabBarController {
    
    init() {
        super.init(nibName:nil, bundle:nil)
        applyStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func applyStyle(){
        self.tabBar.tintColor = UIColor.lmaVerdeLeroy
        self.tabBar.backgroundColor = UIColor.white
        self.tabBar.isTranslucent = false
    }
}
