import Foundation
import UIKit

protocol RouterType: class {
    func start()
    func didFinish()
    func add(child: RouterType)
    func remove(child: RouterType)
}

class Router: NSObject, RouterType {

    private var parentRouter: RouterType?
    private var childRouters: [RouterType] = []

	weak var navigationController: UINavigationController?
	
	init(navigationController: UINavigationController?) {
		self.navigationController = navigationController	}
	
	deinit {
		print("deinit router")
	}

	func start() {
        fatalError("not implemented")
    }

	func didFinish() {
        parentRouter?.remove(child: self)
    }

    final func add(child: RouterType) {
        childRouters.append(child)

        if let impl = child as? Router {
            impl.parentRouter = self
        }
    }

    final func remove(child: RouterType) {
        let index = childRouters.index {
            $0 === child
        }

        if let index = index {
            print("removing router \(child)")
            childRouters.remove(at:index)
        }
    }
}
