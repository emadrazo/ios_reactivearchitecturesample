import lmProduct
import lmCore
import RxSwift

import Foundation


class GetProductDetailUseCase<T> where T:ProductDetailEntityType {
	
	let productService: ProductService<T>
	
	init(productService: ProductService<T>) {
		self.productService = productService
	}
	
	func execute(parameters: GetProductDetailParameters) -> Observable<Transaction<T>> {
		return productService.getProduct(parameters: parameters)
	}
}
