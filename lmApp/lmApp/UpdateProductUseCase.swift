import lmProduct
import lmCore
import RxSwift

import Foundation

class UpdateProductUseCase<T> where T:ProductDetailEntityType {
	
	let service: ProductService<T>
	
	init(productService: ProductService<T>) {
		self.service = productService
	}
	
	func execute(withParameters parameters: UpdateProductParameters<T>) -> Observable<Transaction<T>> {
		return service.updateProduct(parameters: parameters)
	}
}
