//
//  AppDelegate.swift
//  lmApp
//
//  Created by Eva Madrazo on 07/04/2017.
//  Copyright © 2017 Leroy Merlin. All rights reserved.
//

import UIKit
import lmCore
import lmProduct
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	
	var router: MainRouter?
	
	func application(_ application: UIApplication,
	                 didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		
		let window = UIWindow(frame: UIScreen.main.bounds)
		
		// TEMP DB INSERTIONS FOR TESTING PURPOSES
		
		let database = DataBaseClient<ProductDetailEntity>()
		let dataBaseRepository: ProductDataBaseRepository = ProductDataBaseRepository(dataBaseClient: database)
		
		let sampleProducts: [ProductDetailEntity] = ProductsArray.all()
		let bag = DisposeBag()
		sampleProducts.forEach { product in
			print(product)
			dataBaseRepository.createOrUpdate(element: product).subscribe().addDisposableTo(bag)
		}
		
		/// --------------------------------------
		
		router = MainRouter(window: window)
		router?.start()
		
		return true
	}
	
	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state.
		// This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message)
		// or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks.
		// Games should use this method to pause the game.
	}
	
	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers,
		// and store enough application state information to restore your application
		// to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead
		// of applicationWillTerminate: when the user quits.
	}
	
	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state;
		//here you can undo many of the changes made on entering the background.
	}
	
	func applicationDidBecomeActive(_ application: UIApplication) {
		// Restart any tasks that were paused (or not yet started) while the application was inactive.
		// If the application was previously in the background, optionally refresh the user interface.
	}
	
	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate.
		// Save data if appropriate. See also applicationDidEnterBackground:.
	}
}

struct ProductsArray {
	
	static func all() -> [ProductDetailEntity] {
		
        let product0:ProductDetailEntity = ProductDetailEntity(sku: "CBX54A",
                                                   categoryId: "1",
                                                   subcategoryId: "1",
                                                   familyId: "1",
                                                   name: "Test product 1",
                                                   desc: "A product for testing purposes",
                                                   image: "https://www.leroymerlin.es/img/r25/44/4410/441007/19730480/19730480_f1.jpg",
                                                   price: 1.27,
                                                   specialPrice: 1.27,
                                                   discountPercentage: 0.0,
                                                   favorite: false)

        let product1:ProductDetailEntity = ProductDetailEntity(sku: "AAX54A",
                                                   categoryId: "1",
                                                   subcategoryId: "1",
                                                   familyId: "1",
                                                   name: "Test product 2",
                                                   desc: "A product for testing purposes",
                                                   image: "https://www.leroymerlin.es/img/r25/60/6003/600306/panel_ducha_solar_espejo_2_hojas/17052595_f1.jpg",
                                                   price: 5.00,
                                                   specialPrice: 2.50,
                                                   discountPercentage: 50.0,
                                                   favorite: false)
		
        let product2:ProductDetailEntity = ProductDetailEntity(sku: "54ABB",
                                                   categoryId: "1",
                                                   subcategoryId: "1",
                                                   familyId: "1",
		                                           name: "Test product 3",
		                                           desc: "A product for testing purposes",
		                                           image: "image 3",
		                                           price: 1.27,
		                                           specialPrice: 1.27,
		                                           discountPercentage: 0.0,
		                                           favorite: false)
		
        let product3:ProductDetailEntity = ProductDetailEntity(sku: "4ATNB",
                                                   categoryId: "1",
                                                   subcategoryId: "1",
                                                   familyId: "1",
		                                           name: "Test product 4",
		                                           desc: "A product for testing purposes",
		                                           image: "image 4",
		                                           price: 5.00,
		                                           specialPrice: 2.50,
		                                           discountPercentage: 50.0,
		                                           favorite: false)
		
        let product4:ProductDetailEntity = ProductDetailEntity(sku: "B5A33",
                                                   categoryId: "1",
                                                   subcategoryId: "1",
                                                   familyId: "1",
		                                           name: "Test product 5",
		                                           desc: "A product for testing purposes",
		                                           image: "image 5",
		                                           price: 1.27,
		                                           specialPrice: 1.27,
		                                           discountPercentage: 0.0,
		                                           favorite: false)
		
        let product5:ProductDetailEntity = ProductDetailEntity(sku: "ABBC32",
                                                   categoryId: "1",
                                                   subcategoryId: "1",
                                                   familyId: "1",
		                                           name: "Test product 6",
		                                           desc: "A product for testing purposes",
		                                           image: "image 6",
		                                           price: 5.00,
		                                           specialPrice: 2.50,
		                                           discountPercentage: 50.0,
		                                           favorite: false)
		
        let product6:ProductDetailEntity = ProductDetailEntity(sku: "MN4FFW",
                                                   categoryId: "1",
                                                   subcategoryId: "1",
                                                   familyId: "1",
		                                           name: "Test product 7",
		                                           desc: "A product for testing purposes",
		                                           image: "image 7",
		                                           price: 1.27,
		                                           specialPrice: 1.27,
		                                           discountPercentage: 0.0,
		                                           favorite: false)
		
        let product7:ProductDetailEntity = ProductDetailEntity(sku: "U80ZZ",
                                                   categoryId: "1",
                                                   subcategoryId: "1",
                                                   familyId: "1",
		                                           name: "Test product 8",
		                                           desc: "A product for testing purposes",
		                                           image: "image 8",
		                                           price: 5.00,
		                                           specialPrice: 2.50,
		                                           discountPercentage: 50.0,
		                                           favorite: false)
		
        let product8:ProductDetailEntity = ProductDetailEntity(sku: "BBA11",
                                                   categoryId: "1",
                                                   subcategoryId: "1",
                                                   familyId: "1",
		                                           name: "Test product 9",
		                                           desc: "A product for testing purposes",
		                                           image: "image 9",
		                                           price: 1.27,
		                                           specialPrice: 1.27,
		                                           discountPercentage: 0.0,
		                                           favorite: false)
		
        let product9:ProductDetailEntity = ProductDetailEntity(sku: "21T&YY",
                                                   categoryId: "1",
                                                   subcategoryId: "1",
                                                   familyId: "1",
		                                           name: "Test product 10",
		                                           desc: "A product for testing purposes",
		                                           image: "image 10",
		                                           price: 5.00,
		                                           specialPrice: 2.50,
		                                           discountPercentage: 50.0,
		                                           favorite: false)
		
		return [product0, product1, product2, product3, product4, product5, product6, product7, product8, product9]
	}
}
